from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    parser.add_option("--remove",
                      action="store_true", dest="remove",default=False,
                      help="remove all files belonging to a batch")
    (options, args) = parser.parse_args()
    
    paths=[]
    batch = Batch(options.name,options.regex,options.simulation_time)
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    
    batch.CollectRuns(paths)
    ordered = batch.Ordered()


    batch.Remove(options.remove)
if __name__ == "__main__":
    sys.exit(main())


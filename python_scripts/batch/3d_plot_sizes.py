from batch_tools import *
##! /usr/bin/python

from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import PolyCollection
from matplotlib.colors import colorConverter
import matplotlib.pyplot as plt

def PlotSizes_3d(batch,file_name_base="global/ancestor_sizes.dat"):
    fig = plt.figure()
    ax = Axes3D(fig)
    cc = lambda arg: colorConverter.to_rgba(arg, alpha=0.1)
    cols = [cc('r'),cc('c'), cc('g'), cc('b'),cc('y'),cc('m')]
    round_color_list = lambda len: [ cols[i%6] for i in range(0,len) ] 
    verts = []
    count=1
    ordered_runs = batch.Ordered(lambda y:Run.AverageFitness(y,_after=-1000,_before=10000))
    for run in ordered_runs:
        data_file = run.GetFile(file_name_base)
        X=np.loadtxt(data_file,usecols=[0])
        Y=np.loadtxt(data_file,usecols=[1])
        xs=[0.]+list(X)+[len(X)]
        ys=[0.]+list(Y)+[0.]
        verts.append(zip(xs,ys))
        count+=1

    print len(verts)
    zs = np.arange(1,count,1.)

    poly = PolyCollection(verts,facecolors = round_color_list(len(zs)))
    poly.set_alpha(0.7)
    ax.add_collection3d(poly, zs=zs, zdir='y')
    ax.plot(np.array([0,10000,10000]),np.array([1,1,len(zs)]),10,color="black",ls="dashed")

    """ set specific axes properties """
    for axis in ax.w_xaxis, ax.w_yaxis, ax.w_zaxis:
        axis.pane.set_visible(False)
        axis.gridlines.set_visible(False)

    ax.w_yaxis.pane.set_visible(False)
    ax.w_yaxis.gridlines.set_visible(False)
    for elt in ax.w_yaxis.get_ticklines() + ax.w_yaxis.get_ticklabels():
          elt.set_visible(False)

    ax.w_xaxis.pane.set_visible(False)
    ax.w_xaxis.gridlines.set_visible(False)

    ax.w_zaxis.pane.set_visible(False)
    ax.w_zaxis.gridlines.set_visible(False)

    ax.set_xlim3d(0, 10000)
    ax.set_ylim3d(0, len(zs))
    ax.set_zlim3d(0, 400)
    
    plt.show()


def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    
    batch = Batch("tfb_dds_pme_fitness_landscape_long_",'(\d)+$',10000)
    batch.SayName()
    batch.CollectRuns(paths)
    batch.Exclude(range(333,390)+[313,326])
    PlotSizes_3d(batch)
    
    
if __name__ == "__main__":
    sys.exit(main())

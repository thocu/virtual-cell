from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
        
    parser = Options()
    parser.add_option("-o", "--knockout-options",
                      action="store", type="string",dest="knockout_options", default="-ah",
                      help="knockout options")
    parser.add_option("--post-switch-analyse",
                      action="store", type="int",dest="post_switch_analysis",
                      help="knockout analyse in interval after the environmental switch")
    parser.add_option("--load-generation-position",
                      action="store", type="int",dest="load_gen_pos", default=-1,
                      help="ordered position of the generation save file to load")
 
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    # if not options.locations:
    for i in range(options.start,options.end):
        paths.append(Location.fromhost("mutant"+str(i)))
    if options.locations:
        for location in options.locations:
            paths.append(Location(location))
    
    batch = Batch(options.name,options.regex,options.simulation_time,
                  options.switch_time,options.switch_delay,
                  restore_dir_name=options.restore_dir,restore_regex=options.restore_regex)
    batch.SayName()
    batch.CollectRuns(paths)

    if options.included:
        include = [ int(_id) for _id in options.included ]    
        batch.Include(include,pos=0)
            
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        #if options.env_config:
        set.ApplyToSet(lambda x: Run.FitBeforeSwitchS(x))
        #else:
        #    set.ApplyToSet(lambda x: Run.FitBefore(x))
        set.ApplyToSet(lambda x: Run.Finished(x))


    if options.knockout and options.env_config:
        '''This option is disfavoured when doing knockout analysis in
        the "perceived" environment, because the options would be
        loaded automatically anyway through the local_k_config
        file. Defaulting to the next option is thus preferred, because
        it makes use of the local_k_config file'''
        set_job = lambda s: parmap(lambda x: Run.KnockoutAnalyse(x,options.knockout_options+
                                                                 " --env-switching=1"+
                                                 " --env-config="+options.env_config+
                                                 " --env-change-time="+str(x.env_switch[1]),
                                                                 generation_pos=options.load_gen_pos),
                                   [(r,) for r in NodeSet.RunIter(s)],NodeSet.free_cores(s))
        print "in env_config option"
        parmap(set_job,[(s,) for s in batch.nodesets],len(batch.nodesets))
    elif options.knockout and options.mutants and options.post_switch_analysis:
        '''Use options mutant-hosts to run knockout analysis jobs on the given mutants'''
        '''Use post_switch_analysis when knockout analysis should be
        done in a given interval after environment switching. '''
        hosts = [ "mutant"+_id for _id in options.mutants ]
        continue_job = lambda x,h: Run.KnockoutAnalyse(x,options.knockout_options+
                                                       " -g"+str(x.env_switch[1]+1)+
                                                       " -e"+str(x.env_switch[1]+options.post_switch_analysis)
                                                       ,host=h)
        batch.RunSequentialOnHosts(hosts,continue_job,[()])
    elif options.knockout and options.mutants:
        '''Use options mutant-hosts to run knockout analysis jobs on the given mutants'''
        hosts = [ "mutant"+_id for _id in options.mutants ]
        continue_job = lambda x,h: Run.KnockoutAnalyse(x,options.knockout_options,host=h)
        batch.RunSequentialOnHosts(hosts,continue_job,[()])
    elif options.knockout:
        set_job = lambda s: parmap(lambda x:
                                   Run.KnockoutAnalyse(x,options.knockout_options),# program="/home/thocu/Projecten/VirtualCell/WorkBench/knockout"
                                   [(r,) for r in NodeSet.RunIter(s)],NodeSet.free_cores(s))
        parmap(set_job,[(s,) for s in batch.nodesets],len(batch.nodesets))

    
    for x in batch.RunIter():
        if x.has_knockout:
            print x.name, "has knockout"

    for x in batch.RunIter():
        if not x.has_knockout:
            print x.name, "has NO knockout"
    
    
if __name__ == "__main__":
    sys.exit(main())


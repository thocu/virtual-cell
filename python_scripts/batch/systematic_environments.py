from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
        
    parser = Options()
    parser.add_option("--new-env-config",
                      action="store", type="string",dest="new_config", default="/home/thocu/WB/config_files/env5.cfg",
                      help="new environment configuration to reload file")
    parser.add_option("--continue-name",
                      action="store", type="string",dest="continue_name", default="restore",
                      help="new environment configuration to reload file")
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    batch = Batch(options.name,options.regex,options.simulation_time,
                  options.switch_time,options.switch_delay,
                  restore_dir_name=options.restore_dir)
    batch.SayName()
    batch.CollectRuns(paths)

    include = [ int(_id) for _id in options.included ]    
    batch.Include(include)
        
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        set.ApplyToSet(lambda x: Run.FitBeforeSwitchS(x))
        set.ApplyToSet(lambda x: Run.Finished(x))

    systematic_env_path = "/home/thocu/WB/config_files/systematic"

    hosts = [ "mutant"+_id for _id in options.mutants ]

    continue_jobs = []
    env_files = [ os.path.join(systematic_env_path,match.group(0)) for match in ( re.compile('systematic_env(\d+).cfg').match(file_)
                                    for file_ in os.listdir(systematic_env_path) ) if match ]
    env_files.sort(key=lambda f: int(re.compile('.*systematic_env(\d+)\.cfg').match(f).group(1)))
    _id = lambda e: re.compile('.*systematic_env(\d+)\.cfg').match(e).group(1)
    systematic_names = [ "switch_to_systematic_"+_id(_file) for _file in env_files ]
    for f in env_files:
        print f

    continue_job = lambda x,env,sys_name,h: Run.Continue(x,options="--env-config="+env+
                                                         " --env-change-time="+str(x.env_switch[1])+
                                                         " --env-switching=1"
                                                         " --generations=30000",
                                                         from_save=os.path.join(x.name,x.env_switch[0]),
                                                         save_dir=sys_name,host=h
                                                         ,overwrite=options.overwrite)
        
    if options.continue_runs:
        batch.RunSequentialOnHosts(hosts,continue_job,zip(env_files,systematic_names))
        
    for x in batch.RunIter():
        if not x.finished:
            print x.name, "did not finish yet"
    
    for x in batch.RunIter():
        if x.fit_before_switch[0]:
            print x.name, "fit", x.fit_before_switch[1]

    
if __name__ == "__main__":
    sys.exit(main())


from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
        
    parser = Options()
    parser.add_option("--continue-name",
                      action="store", type="string",dest="continue_name", default="continued",
                      help="name of folder where run is continued")
    parser.add_option("--continue-until",
                      action="store", type="int", dest="continue_until", default=30000,
                      help="the end generation of the continue run")
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    batch = Batch(options.name,options.regex,options.simulation_time,
                  options.switch_time,options.switch_delay,
                  restore_dir_name=options.restore_dir)
    batch.SayName()
    batch.CollectRuns(paths)

    if options.included:
        include = [ int(_id) for _id in options.included ]    
        batch.Include(include)
    
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        print "finding switch point"
        set.ApplyToSet(lambda x: Run.FitBeforeSwitchS(x))
        set.ApplyToSet(lambda x: Run.Finished(x))

    # switch to new environment
    # give delay for switching

    hosts = [ "mutant"+_id for _id in options.mutants ] 
    if options.continue_runs:

        continue_job = lambda x,h: Run.Continue(x," --generations="+str(options.continue_until)+
                                                " --env-switching=0",
                                                from_save=os.path.join(x.name,x.env_switch[0]),
                                                save_dir=options.continue_name,host=h
                                                ,overwrite=options.overwrite)
        batch.RunSequentialOnHosts(hosts,continue_job,[()])
        
            
    for x in batch.RunIter():
        if not x.finished:
            print x.name, "did not finish yet"
    
    for x in batch.RunIter():
        if x.fit_before_switch[0]:
            print x.name, "fit", x.fit_before_switch[1]

    
if __name__ == "__main__":
    sys.exit(main())


from batch_tools import *
from scipy import stats

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    parser.add_option("--no-S-for-switch",
                      action="store_true", dest="no_S", default=False,
                      help="the switching generation is not marked with S")
    parser.add_option("-t", "--readapt-threshold",
                      action="store", type="float",dest="readapt_threshold", default=0.85,
                      help="when to call lineage fit after switch")
    parser.add_option("--fit-before", action="store", type="int",dest="fit_before",
                      help="when to call lineage fit after switch")
    parser.add_option("--fit-again-before", action="store", type="int",dest="fit_again_before",
                      help="when to call lineage fit after switch")
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    #if not options.locations:
    for i in range(options.start,options.end):
        paths.append(Location.fromhost("mutant"+str(i)))
    if options.locations:
        for location in options.locations:
            paths.append(Location(location))


    '''When we compare batches with different switch delays we should
    ensure every setting has and equal time frame to adapt to pre and
    post switch environments'''
    fit_before = options.switch_time
    if options.fit_before:
        fit_before = options.fit_before
    fit_again_before = options.simulation_time
    if options.fit_again_before:
        fit_again_before = options.fit_again_before
    
    batch = Batch(options.name,options.regex,options.simulation_time,
                  options.switch_time,options.switch_delay,
                  restore_dir_name=options.restore_dir,restore_regex=options.restore_regex)
    batch.SayName()
    batch.CollectRuns(paths)
    ## for run in batch.RunIter():
##         print run.GraceSave()

    
    ''' Functions that do NOT depend on the knockout  '''
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        if options.no_S:
            set.ApplyToSet(Run.FitBeforeSwitch)
        else:
            set.ApplyToSet(Run.FitBeforeSwitchS)
        set.ApplyToSet(Run.ToSwitch)
        set.ApplyToSet(lambda x: Run.Finished(x))


        
    ''' Functions that depend on the knockout run being done '''    
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.FitAfterSwitch(x,threshold=options.readapt_threshold))


    fit_runs = [x for x in batch.RunIter() if x.fit_before_switch[0] and
                x.has_knockout and x.fit_before_switch[1] <= fit_before ]
    unfit_runs = [ x for x in batch.RunIter() if not x.fit_before_switch[0] and x.to_switch and x.has_knockout]

    print sum(x.fit_before_switch[0] for x in fit_runs ),"fits have",
    print sum(x.Wgds(before=x.env_switch[1],save_dir="global_before/") for x in fit_runs  ),"wgds before switch"

    fit_dat = [x.Wgds(before=x.env_switch[1],save_dir="global_before/") for x in fit_runs ]

    print "fit_wgds",fit_dat
   
    print sum(1 for x in unfit_runs ), "unfits have",
    print sum( x.Wgds(before=x.env_switch[1],save_dir="global_before/")for  x in unfit_runs), "wgds before switch"

    unfit_dat = [x.Wgds(before=x.env_switch[1],save_dir="global_before/") for x in unfit_runs ]

    print "unfit wgds", unfit_dat
    print stats.mannwhitneyu(fit_dat,unfit_dat)

    unfits_with_wgds = sum (1 for x in unfit_runs if x.Wgds(before=x.env_switch[1], save_dir="global_before/") )
    print unfits_with_wgds, "out of", len(unfit_runs), "unfit runs have one or more WGD"

    fits_with_wgds = sum (1 for x in fit_runs if x.Wgds(before=x.env_switch[1],save_dir="global_before/") )
    print fits_with_wgds, "out of", len(fit_runs), "fit runs have one or more WGD"
               

    figure()
    boxplot([fit_dat,unfit_dat], notch=0, sym='+', vert=1, whis=1.5,
        positions=None, widths=None, patch_artist=False,bootstrap=1000)
    savefig("fit_unfit_wgds.svg",format="svg")
    show()

    """max sizes """

    print 
    fit_dat = [x.MaxSize(_before=1000,size_file="global_before/ancestor_sizes.dat")[0]
               for x in fit_runs]

    print "fit_sizes",fit_dat
    try:
        print "fit average", float(sum(fit_dat))/float(len(fit_dat))
    except ZeroDivisionError:
        print "no fits"
   
    print
    unfit_dat = [x.MaxSize(_before=1000,size_file="global_before/ancestor_sizes.dat")[0]
                 for x in unfit_runs ]
    print "unfit sizes", unfit_dat
    try:
        print "unfit average", float(sum(unfit_dat))/float(len(unfit_dat))
    except ZeroDivisionError:
        print "no unfit"
    print stats.mannwhitneyu(fit_dat,unfit_dat)
    figure()
    boxplot([fit_dat,unfit_dat], notch=0, sym='+', vert=1, whis=1.5,
        positions=None, widths=None, patch_artist=False,bootstrap=1000)
    savefig("fit_unfit_sizes.svg",format="svg")
 #   show()

            
    fit_fit_runs = [ x for x in fit_runs if x.fit_after_switch[0] and x.fit_after_switch[1] < fit_again_before ]# - x.fit_before_switch[1] < x.switch_time]
    fit_unfit_runs = [ x for x in fit_runs if not x.fit_after_switch[0]]
    unfit_fit_runs = [ x for x in unfit_runs if x.fit_after_switch[0] and x.fit_after_switch[1] ] #- x.env_switch[1] < x.switch_time] 
    unfit_unfit_runs = [ x for x in unfit_runs if not x.fit_after_switch[0]]

    print "fit-fits",[ x.index for x in fit_fit_runs ]
    print "fit-unfits", [ x.index for x in fit_unfit_runs ]
    print "unfit-fits",[ x.index for x in unfit_fit_runs ]
    print "unfit-unfit:",[ x.index for x in unfit_unfit_runs ]
    print
    
    fit_before_fit_after_wgds = [x.Wgds("global_after/") for x in fit_fit_runs ]                                
    fit_before_unfit_after_wgds = [x.Wgds("global_after/") for x in fit_unfit_runs ]
    
    unfit_before_fit_after_wgds = [x.Wgds("global_after/") for x in unfit_fit_runs ]
    unfit_before_unfit_after_wgds = [x.Wgds("global_after/") for x in unfit_unfit_runs ]

    print len(fit_fit_runs) + len(unfit_fit_runs), "fit after environment switch"

    print
    print "wgds of", len(fit_before_fit_after_wgds), "fit fit runs:",sum(fit_before_fit_after_wgds)
    print "wgds of", len(fit_before_unfit_after_wgds), "fit unfit runs:",sum(fit_before_unfit_after_wgds)
    print "wgds of", len(unfit_before_fit_after_wgds), "unfit fit runs:",sum(unfit_before_fit_after_wgds)
    print "wgds of", len(unfit_before_unfit_after_wgds), "unfit unfit runs:",sum(unfit_before_unfit_after_wgds)
    print


    time_to_readapt_fit_dat = [ x.fit_after_switch[1] - x.env_switch[1] for x in fit_fit_runs ]
    time_to_readapt_unfit_dat = [ x.fit_after_switch[1] - x.env_switch[1] for x in unfit_fit_runs ]
                                 
    print "readaptation times for ", len(time_to_readapt_fit_dat), "fit, readapted runs and", len(time_to_readapt_unfit_dat), "unfit, readapted runs"
    try:
        print "fit average", float(sum(time_to_readapt_fit_dat))/float(len(time_to_readapt_fit_dat)), "generations"
    except ZeroDivisionError:
        print "no fits"
    try:
        print "unfit average", float(sum(time_to_readapt_unfit_dat))/float(len(time_to_readapt_unfit_dat)), "generations"
    except ZeroDivisionError:
        print "no unfits"
    figure()
    boxplot([time_to_readapt_fit_dat,time_to_readapt_unfit_dat], notch=0, sym='+', vert=1, whis=1.5,
        positions=None, widths=None, patch_artist=False,bootstrap=1000)
    savefig("readapt_times.svg",format="svg")
#    show()

    
    print "quick adaptation:", sum(1 for x in fit_fit_runs if x.fit_before_switch[1] <= 1000)
    print "quick readaptation:", sum(1 for x in fit_fit_runs if x.fit_after_switch[1] - x.env_switch[1] <= 1000)

    fit_wgd_list_before = [ x.WgdList("global_before") for x in fit_runs ]
    unfit_wgd_list_before = [ x.WgdList("global_before") for x in unfit_runs ]

    wgd_dist_file = open("fits_wgd_dist_before.dat",'w')

    for run_dat in fit_wgd_list_before:
        for (gen,wgds) in run_dat:
            print >> wgd_dist_file , str(gen),str(wgds)
    wgd_dist_file.close()

    wgd_dist_file = open("unfits_wgd_dist_before.dat",'w')

    for run_dat in unfit_wgd_list_before:
        for (gen,wgds) in run_dat:
            print >> wgd_dist_file , str(gen),str(wgds)
    wgd_dist_file.close()

    fit_wgd_list_after = [ (x,x.WgdList("global_after")) for x in fit_runs ]
    unfit_wgd_list_after = [ (x,x.WgdList("global_after")) for x in unfit_runs ]

    wgd_dist_file = open("fits_wgd_dist_after.dat",'w')

    for run,run_dat in fit_wgd_list_after:
        switch_point = run.env_switch[1]
        for (gen,wgds) in run_dat:
            print >> wgd_dist_file , str(gen-switch_point),str(wgds)
    wgd_dist_file.close()

    wgd_dist_file = open("unfits_wgd_dist_after.dat",'w')

    for run,run_dat in unfit_wgd_list_after:
        switch_point = run.env_switch[1]
        for (gen,wgds) in run_dat:
            print >> wgd_dist_file , str(gen-switch_point),str(wgds)
    wgd_dist_file.close()

    
    print fit_wgd_list_before
    print unfit_wgd_list_before
    
    for x in batch.RunIter():
        if not x.finished:
            print x.name, "did not finish yet"
    
    for x in batch.RunIter():
        if x.fit_before_switch[0]:
            print x.name, "fit", x.fit_before_switch[1]
    
if __name__ == "__main__":
    sys.exit(main())


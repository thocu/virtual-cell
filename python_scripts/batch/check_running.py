from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    
    batch = Batch(options.name,"(\d)+$",options.simulation_time,options.switch_time,
                  restore_dir_name=options.restore_dir,restore_regex=options.restore_regex)
    batch.SayName()
    batch.CollectRuns(paths)
        
    for set in batch.SetIter():
        print set.location.dir_
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        set.ApplyToSet(lambda x: Run.FitBefore(x))
        set.ApplyToSet(lambda x: Run.FitBeforeSwitchS(x))
        set.ApplyToSet(lambda x: Run.Finished(x))
        set.ApplyToSet(lambda x: Run.AtGeneration(x))

    print
    for x in batch.RunIter():
        if not x.finished:
            print x.index, "did not finish yet"
    
    for x in batch.RunIter():
        if x.fit_before_switch[0]:
            print x.index, "fit", x.fit_before_switch[1]
    
if __name__ == "__main__":
    sys.exit(main())


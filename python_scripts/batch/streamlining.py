##! /usr/bin/python
from batch_tools import *
import random, scipy
from itertools import *
from scipy import stats
import matplotlib.pyplot as plt





def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end

    for i in range(options.start,options.end):
        paths.append(Location.fromhost("mutant"+str(i)))

    paths.append(Location("/home/thocu/thocu2/mutant6_2011_08_11"))
    paths.append(Location("/home/thocu/thocu2/mutant4_2011_11_24"))

    out = open("test_streamlining.dat",'w')
   
    batch1 = Batch("tfb_dds_pme_large_pop_",'(\d)+$',10000)
    batch1.SayName()
    batch1.CollectRuns(paths)
    for set in batch1.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        set.ApplyToSet(lambda x: Run.FitBefore(x,fitness_file="global/ancestor_standard_fitnesses.dat",before=-5000))
        set.ApplyToSet(lambda x: Run.Finished(x))

    mins1 = []
    maxes1 = []
    for run in batch1.RunIter():
        if run.is_fit : #and run.finished :
            mins1.append(run.MinSize(_after=run.fit_at[1],_before=run.fit_at[1]+5000))
            #            maxes1.append(run.MaxSize(_after=run.fit_at[1],_before=run.fit_at[1]+5000))

    count = 0
    for run in batch1.RunIter():
        if run.is_fit : #and run.finished :
            generation = mins1[count][0]            
            maxes1.append(run.MaxSize(_after=mins1[count][1],_before=run.fit_at[1]+5000))
            count += 1

    batch2 = Batch("tfb_dds_pme_fitness_landscape_long_",'(\d)+$',10000)
    batch2.SayName()
    batch2.CollectRuns(paths)
    for set in batch2.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        set.ApplyToSet(lambda x: Run.FitBefore(x,before=-5000))
        set.ApplyToSet(lambda x: Run.Finished(x))

    mins2 = []
    maxes2 = []
    for run in batch2.RunIter():
        if run.is_fit and run.finished :
            mins2.append(run.MinSize(_after=run.fit_at[1],_before=run.fit_at[1]+5000))
     #       maxes2.append(run.MaxSize(_after=run.fit_at[1],_before=run.fit_at[1]+5000))

    count = 0    
    for run in batch2.RunIter():
        if run.is_fit and run.finished :
            maxes2.append(run.MaxSize(_after=mins2[count][1],_before=run.fit_at[1]+5000))
            count += 1

    print len(mins1), "minima for large pop.", len(mins2), "minima for medium pop"

    print "large pop min, mean/std", scipy.mean([min_[0] for min_ in mins1]), scipy.std([min_[0] for min_ in mins1])
    print "medium pop min, mean/std", scipy.mean([min_[0] for min_ in mins2]), scipy.std([min_[0] for min_ in mins2])
    print stats.mannwhitneyu([min_1[0] for min_1 in mins1],[min_2[0] for min_2 in mins2])

    print len(mins1), "minima for large pop.", len(mins2), "minima for medium pop"
    
    print
    print
    print "large pop max, mean/std", scipy.mean([max_[0] for max_ in maxes1]), scipy.std([max_[0] for max_ in maxes1])
    print "medium pop max, mean/std", scipy.mean([max_[0] for max_ in maxes2]), scipy.std([max_[0] for max_ in maxes2])
    print stats.mannwhitneyu([max_1[0] for max_1 in maxes1],[max_2[0] for max_2 in maxes2])

    print

    for max_ in maxes1:
        print >> out, 0,max_[0]
 #       out.write(str(max_[0])+'\n')

    out.write('\n')
    for max_ in maxes2:
        print >> out, 1,max_[0]
#        out.write(str(max_[0])+'\n')
    
    out.close()
if __name__ == "__main__":
    sys.exit(main())



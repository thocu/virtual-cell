from batch_tools import *

'''compare systematic environments side by side'''

class site:
    def __init__(self,a):
        self.a = a
    def set_a(self, new_a):
        self.a = new_a

def systematic_index(name,pattern='_(\d+)$'):
        p = re.compile(pattern)
        m = p.search(name)
        if m is not None:
            index = int(m.group(1))
        else:
            self.index = -1
            print "no match"
        return index


class SystematicMatrix():

    def __init__(self,batches,ordering_key=lambda y :systematic_index(y.name),size=80):
        '''the default size for a batch in the SysM (80) reflects the
        number of systematic environments typical for this specific
        simulation'''
        lattice = np.empty((0,size), dtype=object)
        for batch in batches:
            if batch.Size() != size:
                batch.SayName()
                print "is not size", size, "but ", batch.Size()
                return
            '''lattice contains the batches, stacked on top of
            eachother, with their Runs sorted numerically by their
            environment indices'''
            lattice = np.vstack((lattice,
                                 batch.Ordered(ordering_key ))) 
        self.lattice = lattice

## def systematic_index(name,pattern='_(\d+)$'):
##         p = re.compile(pattern)
##         m = p.search(name)
##         if m is not None:
##             index = int(m.group(1))
##         else:
##             self.index = -1
##             print "no match"
##         return index


## class SystematicMatrix():

##     def __init__(self,batches,size=80):
##         lattice = np.empty((0,size), dtype=object)
##         for batch in batches:
##             if batch.Size() != size:
##                 print batch.SayName(), "is not size", size
##                 return
##             lattice = np.vstack((lattice,batch.Ordered(lambda y: systematic_index(y.name) ))) 
##         self.lattice = lattice




def main(argv=None):
    if argv is None:
        argv = sys.argv
        
    parser = Options()
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    for i in range(options.start,options.end):
        paths.append(Location.fromhost("mutant"+str(i)))
    if options.locations:
        for location in options.locations:
            paths.append(Location(location))

    batch_list=[]
    for i in options.included:
        
        batch = Batch(options.name,i+'$',options.simulation_time,
                      options.switch_time,options.switch_delay,
                      restore_dir_name=options.restore_dir,restore_regex='\d+$')
        batch.SayName()
        batch.CollectRuns(paths)
        batch_list.append(batch)

    Matrix = SystematicMatrix(batch_list)

    def print_index(run):
        return systematic_index(run.name)

    vindex = np.vectorize(print_index)
    vaverage_fit = np.vectorize(lambda x: Run.AverageFitness(x,_after=25000))
    print
    C = vindex(Matrix.lattice)
    AV = vaverage_fit(Matrix.lattice)
    print C
    print AV
    print np.transpose(AV)
    AVenv = np.average(AV,axis=0)
    print AVenv
    
if __name__ == "__main__":
    sys.exit(main())


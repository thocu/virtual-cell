from batch_tools import *
from scipy import stats

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    parser.add_option("--no-S-for-switch",
                      action="store_true", dest="no_S", default=False,
                      help="the switching generation is not marked with S")
    parser.add_option("-t", "--readapt-threshold",
                      action="store", type="float",dest="readapt_threshold", default=0.85,
                      help="when to call lineage fit after switch")
    parser.add_option("--fit-before", action="store", type="int",dest="fit_before",
                      help="when to call lineage fit after switch")
    parser.add_option("--fit-again-before", action="store", type="int",dest="fit_again_before",
                      help="when to call lineage fit after switch")
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))

    '''When we compare batches with different switch delays we should
    ensure every setting has and equal time frame to adapt to pre and
    post switch environments; Hence if we want to maximally compare
    between long and short streamlining, fit_before should be
    switch-time - max(switch_delay1,switch_delay2). Likewise,
    fit_again_before should be set to simulation-time - |switch_delay1
    - switch_delay2 | in case of the simulation with short delay, lest
    the fast switcher has more time to evolve high fitness'''
    fit_before = options.switch_time
    if options.fit_before:
        fit_before = options.fit_before
    fit_again_before = options.simulation_time
    if options.fit_again_before:
        fit_again_before = options.fit_again_before
    
    batch = Batch(options.name,options.regex,options.simulation_time,
                  options.switch_time,options.switch_delay,
                  restore_dir_name=options.restore_dir)
    batch.SayName()
    batch.CollectRuns(paths)

    ''' Functions that do NOT depend on the knockout  '''
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        if options.no_S:
            set.ApplyToSet(Run.FitBeforeSwitch)
        else:
            set.ApplyToSet(Run.FitBeforeSwitchS)
        set.ApplyToSet(Run.ToSwitch)
        set.ApplyToSet(lambda x: Run.Finished(x))
        
    ''' Functions that depend on the knockout run being done '''    
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.FitAfterSwitch(x,threshold=options.readapt_threshold))

    fit_runs = [x for x in batch.RunIter() if x.fit_before_switch[0] and
                x.has_knockout and x.fit_before_switch[1] <= fit_before ]
    unfit_runs = [ x for x in batch.RunIter() if not x.fit_before_switch[0] and x.to_switch and x.has_knockout]
            
    fit_fit_runs = [ x for x in fit_runs if x.fit_after_switch[0] and x.fit_after_switch[1] < fit_again_before ]
    fit_unfit_runs = [ x for x in fit_runs if not x.fit_after_switch[0]]
    unfit_fit_runs = [ x for x in unfit_runs if x.fit_after_switch[0] ]
    unfit_unfit_runs = [ x for x in unfit_runs if not x.fit_after_switch[0]]
    fit_fit_runs.sort(key=lambda x: x.index)
    fit_unfit_runs.sort(key=lambda x: x.index)
    unfit_fit_runs.sort(key=lambda x: x.index)
    unfit_unfit_runs.sort(key=lambda x: x.index)
    print "fit-fits",[ x.index for x in fit_fit_runs ]
    print "fit-unfits", [ x.index for x in fit_unfit_runs ]
    print "unfit-fits",[ x.index for x in unfit_fit_runs ]
    print "unfit-unfit:",[ x.index for x in unfit_unfit_runs ]
    print
    
    print len(fit_fit_runs) + len(unfit_fit_runs), "fit after environment switch"

    print

    time_to_readapt_fit_dat = [ x.fit_after_switch[1] - x.env_switch[1] for x in fit_fit_runs ]
    time_to_readapt_unfit_dat = [ x.fit_after_switch[1] - x.env_switch[1] for x in unfit_fit_runs ]
                                 
    print "readaptation times for ", len(time_to_readapt_fit_dat), "fit, readapted runs and", len(time_to_readapt_unfit_dat), "unfit, readapted runs"
    print "fit average", float(sum(time_to_readapt_fit_dat))/float(len(time_to_readapt_fit_dat)), "generations"
    print "unfit average", float(sum(time_to_readapt_unfit_dat))/float(len(time_to_readapt_unfit_dat)), "generations"
    figure()
    boxplot([time_to_readapt_fit_dat,time_to_readapt_unfit_dat], notch=0, sym='+', vert=1, whis=1.5,
        positions=None, widths=None, patch_artist=False,bootstrap=1000)
    savefig("readapt_times.svg",format="svg")
#    show()

    
    print "quick adaptation:", sum(1 for x in fit_fit_runs if x.fit_before_switch[1] <= 1000)
    print "quick readaptation:", sum(1 for x in fit_fit_runs if x.fit_after_switch[1] - x.env_switch[1] <= 2000)

    print
    print "size differences:"
    for run in fit_runs:
        switch_gen = run.env_switch[1] 
        at_gen = run.env_switch[1] - 500
        print  run.MaxSize(size_file="global_before/ancestor_sizes.dat",_before=at_gen,_after=at_gen-1)[0] - run.MaxSize(size_file="global_before/ancestor_sizes.dat",_before=switch_gen,_after=switch_gen-1)[0]
            
    for x in batch.RunIter():
        if not x.finished:
            print x.name, "did not finish yet"
    
    for x in batch.RunIter():
        if x.fit_before_switch[0]:
            print x.name, "fit", x.fit_before_switch[1]
    
if __name__ == "__main__":
    sys.exit(main())


from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
        
    parser = Options()
    parser.add_option("--new-env-config",
                      action="store", type="string",dest="new_config", default="/home/thocu/WB/config_files/env5.cfg",
                      help="new environment configuration to reload file")
    parser.add_option("--continue-name",
                      action="store", type="string",dest="continue_name", default="restore",
                      help="new environment configuration to reload file")
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    batch = Batch(options.name,options.regex,options.simulation_time,
                  options.switch_time,options.switch_delay,
                  restore_dir_name=options.restore_dir)
    batch.SayName()
    batch.CollectRuns(paths)
        
    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        if options.restore_dir:
            set.ApplyToSet(lambda x: Run.ReloadSwitchTime(x))
        elif options.new_config:
            print "finding switch point"
            set.ApplyToSet(lambda x: Run.FitBeforeSwitchS(x))
        set.ApplyToSet(lambda x: Run.Finished(x))

    # switch to new environment
    # give delay for switching

    hosts = [ "mutant"+_id for _id in options.mutants ] 
    if options.continue_runs:

        continue_job = lambda x,h: Run.Continue(x,"--env-config="+options.new_config+
                                                          " --env-change-time="+str(x.env_switch[1])+
                                                          " --env-switching=1"
                                                          " --generations=30000",
                                                          from_save=os.path.join(x.name,x.env_switch[0]),
                                                          save_dir=options.continue_name,host=h
                                                          )
        batch.RunOnHosts(hosts,continue_job)
        
##         set_job = lambda s: parmap(lambda x: Run.Continue(x,"--env-config="+options.new_config+
##                                                           " --env-change-time="+str(x.env_switch[1])+
##                                                           " --env-switching"
##                                                           " --generations=30000",
##                                                           from_save=os.path.join(x.name,x.env_switch[0]),
##                                                           save_dir="test_continue"
##                                                           ),
##                                [r for r in NodeSet.RunIter(s) ],NodeSet.free_cores(s))
##         parmap(set_job,batch.nodesets,len(batch.nodesets))
            
    for x in batch.RunIter():
        if not x.finished:
            print x.name, "did not finish yet"
    
    for x in batch.RunIter():
        if x.fit_before_switch[0]:
            print x.name, "fit", x.fit_before_switch[1]

    
if __name__ == "__main__":
    sys.exit(main())


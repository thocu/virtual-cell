##! /usr/bin/python
import re, sys, os, shutil,shlex, subprocess,re
from random import shuffle
import numpy as np
from optparse  import OptionParser
#from multiprocessing import Pool
import multiprocessing
import math
import time
from multiprocessing import Process, Pipe , Pool
import itertools
from itertools import izip, cycle
#from matplotlib.pyplot import boxplot
from pylab import *
from functools import partial
import collections,cStringIO
from IPython.parallel import interactive

knockout="nice /home/thocu/WB/knockout"
env_change="nice /home/thocu/WB/winds_of_change"
local_saves="/home/thocu/WB/python_scripts/local_files/"

def rev_readlines3(arg,bufsize=8192):
    f1=open(arg,'rb')
    f1.seek(0,2)# go to the end
    leftover=''
    while f1.tell():
        if f1.tell()<bufsize: bufsize=f1.tell()
        f1.seek(-bufsize,1)
        in_memory=f1.read(bufsize)+leftover
        f1.seek(-bufsize,1)
        lines=in_memory.split('\n')
        for i in reversed(lines[1:]): yield i
        leftover=lines[0]
    yield leftover
    
class Location:
    prefix = "/hosts/linuxhome/"
    postfix = "/tmp/thomas/"

    def __init__(self,location,host="memory"):
        self.host = host
        self.dir_ = location

        
    @classmethod
    def fromhost(cls,host,prefix=None,postfix=None):
        if prefix is None:
            prefix = cls.prefix
        if postfix is None:
            postfix = cls.postfix
        return cls(prefix+host+postfix,host)

    
def group3(lst, n):
    """group([0,3,4,10,2,3], 2) => iterator

    Group an iterable into an n-tuples iterable. Incomplete tuples
    are discarded e.g.

    >>> list(group(range(10), 3))
    [(0, 1, 2), (3, 4, 5), (6, 7, 8)]
    """
    return itertools.izip(*[itertools.islice(lst, i, None, n) for i in range(n)])

def Generation(file_name,generation_naming):
    return int(file_name.split('/')[-1].strip(generation_naming).rstrip('S'))

def myround_up(x, base):
    return int(base * math.ceil(float(x)/base))

            
class Run():
    '''Hold information on a single run provides methods to access various run results'''
    ## name = ""
##     host = ""
##     has_knockout = False
##     env_switch = (None,-1)
    def __init__(self,name,host,switch_time,simulation_time,switch_delay,parent=None):
        '''rationale behind the __init__ checks:  http://code.activestate.com/recipes/
        52236-various-idioms-for-call-my-superclasss-__init__-if/ '''
        for X in self.__class__.__bases__:
            fun = getattr(X, '__init__', lambda x: None)
            fun(self)
        self.name = name
        self.host = host
        self.generation_saves = [] 
        self.generation_naming = "generation"
        self.switch_time = switch_time
        self.has_knockout = False
        if os.path.exists(name+'/global_after/ancestor_standard_fitnesses.dat'):
            '''this file should at least exist to obtain useful knockout statistics'''
            self.has_knockout = True
        self.env_switch = (None,self.switch_time)
        self.to_switch = False
        self.fit_before_switch = (False, -1)
        self.fit_after_switch = (False, -1)
        self.has_saves = False
        self.simulation_time=simulation_time
        self.switch_delay=switch_delay
        self.finished = False
        self.parent=parent
        self.index=[]
        self.ExtractIndex()

    def ExtractIndex(self,pattern='_(\d+)(/|$)'): #((/.\D+$)|$)'):
        p = re.compile(pattern)
        m = p.search(self.name)        
        for m in p.finditer(self.name):
            self.index.append(int(m.group(1)))
        return self.index

    def GetFile(self,file_name_base):
        file_path = os.path.join(self.name,file_name_base)
        if not os.path.exists(file_path):
            print "file ", file_name_base, "not found for", self.name
            return None
        return file_path        
                    
                
    def GraceSave(self,grace_file="graceplot.agr"):
        '''return the gracefile of this run'''
        return self.GetFile(grace_file)

    def GenerationSaves(self,regex="^generation(\d)+S?$"):
        '''find and sort the generation saves of the run, including the "S"-versions'''
        gen_regex = re.compile(regex)
        self.generation_saves=[]
        if self.parent:
            self.generation_saves += [ match.group(0) for match
                                      in ( gen_regex.match(file_) for file_
                                           in os.listdir(self.parent)) if match ]
            
        self.generation_saves += [ match.group(0) for match
                                  in ( gen_regex.match(file_) for file_
                                       in os.listdir(self.name)) if match ]
        self.generation_saves.sort(key= lambda name: Generation(name,self.generation_naming))
        if len(self.generation_saves) > 0 :
            self.has_saves = True
        return self.generation_saves
                

    def KnockoutAnalyse(self,options="-ah",host=None,program=knockout,
                        generation_pos=-1,redirect=None):
        '''call the knockout executable on a specified generation
        (index) of this run (default is last)'''
        if host is None:
            host = self.host
            print "knockout", self.name.split("/")[-1], "on", host
        if redirect is None:
            log_file = os.path.normpath(os.path.join(self.name,"knock.out"))
            redirect = ">& "+log_file 
        if self.has_saves:
            load_file = os.path.join(self.name,self.generation_saves[generation_pos])
            print "starting knockout of", self.name, "with options", options, "on", host
            retcode = subprocess.call(["ssh",host,program,options,
                                       load_file,redirect])
            print "return code",retcode, "for", self.name
            if int(retcode) == 0 :
                print self.name, "has knockout"
                self.has_knockout = True
        else:
            print "no saves found to start knockout analysis"

    def Continue(self,options,host=None,from_save=None,silent=True,
                 program=env_change,redirect=None,save_dir=None,overwrite=False):
        '''continuation of a run; motivation: continue populations
        saved at the environment switch point, but postponing the
        environmental switch. Can be used for any type of continuation.'''
        if save_dir is None:
            save_dir = os.path.join(self.name,"restore_dir")
        else:
            save_dir = os.path.join(self.name,save_dir)
        if host is None:
            host = self.host
        if from_save is None:            
            from_save = os.path.join(self.name,self.env_switch[0])
        if redirect is None:
            log_base = os.path.normpath(save_dir)
            redirect = ">& "+log_base+".out "
        if os.path.exists(save_dir) and (not overwrite):
            print save_dir, "exists and not overwriting; skipping job"
        else:
            print "starting run saving to", save_dir
            retcode = subprocess.call(["ssh",host,"exec",program,options,"-l "+from_save,
                                       "--save-dir="+save_dir,redirect])
            if int(retcode) == 0 :
                print "finished run saved in", save_dir
        
    def Finished(self,generations=None,reference_file="final_fitnesses.dat"):
        '''Test if this run has reached a given generation.'''
        if generations is None:
            generations = self.simulation_time
        data_file = os.path.join(self.name,reference_file)
        if not os.path.exists(data_file):
            print "no reference file for finished checking found for:", self.name
            return None
        for line in open(data_file):
            generation = int( line.split()[0])
            if (generation >= generations):
                self.finished = True
        return self.finished

    def AtGeneration(self,reference_file="final_fitnesses.dat"):
        '''Output at which generation this run is. '''
        data_file = os.path.join(self.name,reference_file)
        if not os.path.exists(data_file):
            print "no reference file for End Of Run checking found for:", self.name
            return None
        line = open(data_file).readlines()[-1]
        generation = int( line.split()[0])
        print self.name, "at generation",generation
                
    def FitBeforeSwitchS(self):
        '''Checks if run became fit by inspecting the "S" postfix of
        generation saves (this only works in env_switch runs). Assumes
        an ordered list of generation saves'''
        fit_at = (False,-1)
        first_fit = True
        for save in self.generation_saves:
            generation = Generation(save,self.generation_naming)
            if save[-1] == "S" and first_fit:
                fit_at = (True,generation)
                print save, "is fit"
                first_fit = False
            if fit_at[0] and generation == fit_at[1] + self.switch_delay:
                '''do not look any further, here was the switch'''
                self.env_switch = save, generation
                self.to_switch = True
                break
            elif generation >= self.switch_time:
                '''regardless of being fit, environment switched at this generation'''
                self.env_switch = save, generation
                self.to_switch = True
                break
        self.fit_before_switch = fit_at
        return self.fit_before_switch

                                      
    ##If it ever needs to be used, fix the assignment of env_switch, using switch_delay
    def FitBeforeSwitch(self,fitness_file="global_before/ancestor_standard_fitnesses.dat",pos=1,
                        threshold=0.85):
        '''Fitness testing before environmental switch'''
        file_path = os.path.join(self.name,fitness_file)
        self.fit_before_switch = self.IsFitFromFile(0,self.switch_time,file_path,
                                               pos,lambda x: x > threshold)
        return self.fit_before_switch
    

    # Currently Not Used 
    def EnvSwitch(self,generation,generation_naming="generation",save_time=500):
        '''give back an environment switch tuple, by looking up the
        nearest dish save, given that a fitness threshold was reached
        at some generation'''
        nearest = myround_up(generation,save_time)
        return generation_naming+str(nearest),nearest

    def IsFitFromFile(self,after,before,file_name,pos,func):
        '''Find generation where threshold function for fitness
        becomes true'''
        fit_at = (False,-1)
        if not os.path.exists(file_name):
            print "no fitness file found for:", self.name
        else:
            for line in open(file_name,'r'):
                splits = line.split()
                generation = int(splits[0])
                if generation <= after:
                    continue
                if generation > before:
                    break
                fitness = float(splits[pos])
                if func(fitness) :
                    fit_at = (True,generation)
                    break
        return fit_at

    def FitBefore(self,fitness_file="final_fitnesses.dat",pos=1,
                  before=None,threshold=0.85):
        '''Fitness testing before a specified time (default is simulation time)'''
        if before is None:
            before = self.simulation_time
        if before < 0:
            before = self.simulation_time + before
        file_path = os.path.join(self.name,fitness_file)
        self.fit_at = self.IsFitFromFile(0,before,file_path,
                                    pos,lambda x: x > threshold)
        self.is_fit = self.fit_at[0]
        return self.fit_at

    def FitAfterSwitch(self,fitness_file="global_after/ancestor_standard_fitnesses.dat",pos=1,
                       threshold=0.85):
        '''Fitness testing after environmental switch'''
        file_path = os.path.join(self.name,fitness_file)
        print file_path
        self.fit_after_switch = self.IsFitFromFile(self.env_switch[1],self.simulation_time,
                                              file_path,pos,lambda x: x > threshold)
        print self.env_switch, self.fit_after_switch
        return self.fit_after_switch    

    def RawVals(self,file_name,pos,after=0,before=None):
        file_path=os.path.join(self.name,file_name)
        if not os.path.exists(file_path):
            print "no file named:", file_name, "found for:", self.name
            return None
        lines = open(file_path,'r').readlines()
        last_gen = int(lines[-1].split()[0])
        if before is None:
            before = self.simulation_time
        if last_gen < before:
            print "No data between",last_gen, "and",before, "for",self.name
        lines = open(file_path,'r').readlines()
        if after < 0 :
            after = max(0,last_gen + after)
        vals = []
        for line in lines:
            splits = line.split()
            generation = int(splits[0])
            if generation <= after:
                continue
            if generation > before:
                break
            vals.append(float(splits[pos]))
        return vals
        

    def Average(self,file_name,pos,after=0,before=None):
        '''Calculate an average for a run in a specific interval.'''
        file_path=os.path.join(self.name,file_name)
        if not os.path.exists(file_path):
            print "no file named:", file_name, "found for:", self.name
            average = float('nan')
        else:
            lines = open(file_path,'r').readlines()
            last_gen = int(lines[-1].split()[0])
            if before is None:
                before = self.simulation_time
            if last_gen < before:
                print "No data between",last_gen, "and",before, "for",self.name
            lines = open(file_path,'r').readlines()
            if after < 0 :
                after = max(0,last_gen + after)
            cumulative = 0
            count = 0
            for line in lines:
                splits = line.split()
                generation = int(splits[0])
                if generation <= after:
                    continue
                if generation > before:
                    break
                cumulative += float(splits[pos])
                count += 1
            if count == 0:
                print "no lines within range:",after,"--",before,"found for",self.name
                average = float('nan')
            else:
                average = float(cumulative)/float(count)
        return average

    def Max(self,file_name,pos,after=0,before=None):
        '''calculate an maximum for a run in a specific interval'''
        file_path=os.path.join(self.name,file_name)
        if not os.path.exists(file_path):
            print "no file named:", file_name, "found for:", self.name
            maximum = float("-inf"),-1
        else:
            lines = open(file_path,'r').readlines()
            last_gen = int(lines[-1].split()[0])
            if before is None:
                before = self.simulation_time
            if last_gen < before:
                print "No data between",last_gen, "and",before, "for", self.name
            if after < 0 :
                after = max(0,last_gen + after)
            maximum = (float("-inf"),-1)
            for line in lines:
                splits = line.split()
                generation = int(splits[0])
                if generation <= after:
                    continue
                if generation > before:
                    break
                value = float(splits[pos])
                if value > maximum[0]:
                    maximum = (value,generation)
        return maximum

    def Min(self,file_name,pos,after=0,before=None):
        '''calculate an minimum for a run in a specific interval'''
        file_path=os.path.join(self.name,file_name)
        if not os.path.exists(file_path):
            print "no file named:", file_name, "found for:", self.name
            minimum = float('inf'),-1
        else:
            lines = open(file_path,'r').readlines()
            last_gen = int(lines[-1].split()[0])
            if before is None:
                before = self.simulation_time
            if last_gen < before:
                print "No data between",last_gen, "and",before, "for", self.name
            if after < 0 :
                after = max(0,last_gen + after)
            minimum = (float('Inf'),-1)
            for line in lines:
                splits = line.split()
                generation = int(splits[0])
                if generation <= after:
                    continue
                if generation > before:
                    break
                value = float(splits[pos])
                if value < minimum[0]:
                    minimum = (value,generation)
        return minimum

    def ValAt(self,file_name,pos,at):
        '''calculate an minimum for a run in a specific interval'''
        val = float('inf')
        file_path=os.path.join(self.name,file_name)
        if not os.path.exists(file_path):
            print "no file named:", file_name, "found for:", self.name
        else:
            lines = open(file_path,'r').readlines()
            last_gen = int(lines[-1].split()[0])
            if last_gen < at:
                print "No data between",last_gen, "and",at, "for", self.name
            for line in lines:
                splits = line.split()
                generation = int(splits[0])
                if generation == at:
                    val = float(splits[pos])
                    break
        return val


    def ReachesValAt(self,value,file_name,pos,after=0,before=None,larger=False):
        '''determine time at which this value is reached or passed'''
        file_path=os.path.join(self.name,file_name)
        at_generation = -1,inf
        if not os.path.exists(file_path):
            print "no file named:", file_name, "found for:", self.name
        else:
            lines = open(file_path,'r').readlines()
            last_gen = int(lines[-1].split()[0])
            if before is None:
                before = self.simulation_time
            if last_gen < before:
                print "No data between",last_gen, "and",before, "for", self.name
            if after < 0 :
                after = max(0,last_gen + after)

            
            pass_value_threshold = lambda x: x <= value
            if larger:
                pass_value_threshold = lambda x : x >= value
            
            for line in lines[1:]:
                splits = line.split()
                generation = int(splits[0])
                if generation <= after:
                    continue
                if generation > before:
                    break
                data_value = float(splits[pos])
                if pass_value_threshold(data_value):
                #if data_value <= value:
                    at_generation = generation,data_value
                    break

        return at_generation

    def AverageFitness(self,fitness_file="final_fitnesses.dat",_after=0,_before=None):
        return self.Average(fitness_file,pos=1,after=_after,before=_before)

    def RawValsFitness(self,fitness_file="final_fitnesses.dat",_after=0,_before=None):
        return self.RawVals(fitness_file,pos=1,after=_after,before=_before)

    def AverageSize(self,size_file="global/ancestor_sizes.dat",_after=0,_before=None):
        return self.Average(size_file,pos=1,after=_after,before=_before)

    def MaxFitness(self,fitness_file="final_fitnesses.dat",_after=0,_before=None):
        return self.Max(fitness_file,pos=1,after=_after,before=_before)

    def MaxSize(self,size_file="global/ancestor_sizes.dat",_after=0,_before=None):
        return self.Max(size_file,pos=1,after=_after,before=_before)

    def MinFitness(self,fitness_file="final_fitnesses.dat",_after=0,_before=None):
        return self.Min(fitness_file,pos=1,after=_after,before=_before)

    def MinSize(self,size_file="global/ancestor_sizes.dat",_after=0,_before=None):
        return self.Min(size_file,pos=1,after=_after,before=_before)

    def LineageSizes(self,size_file="global/ancestor_sizes.dat",_after=0,steps=15,stepsize=1000):
        sizes = None
        file_path=os.path.join(self.name,size_file)
        if not os.path.exists(file_path):
            print "no file named:", size_file, "found for:", self.name
        else:
            f = open(file_path,'r')
            sizes = []
            for t in range(_after,_after+1+steps*stepsize,stepsize):
                while True:
                    line = f.readline()
                    if not line:
                        sizes.append(float('inf'))
                        break
                    splits = line.split()
                    generation = int(splits[0])
                    if generation < t:
                        continue
                    if generation == t:
                        sizes.append(int(splits[1]))
                        break
        return sizes

    def LossRate(self,size_file,after,before,loss_fraction=0.5):
        '''Take the maximum size and the subsequent minimum size in a
        time interval to calculate the rate of gene loss.'''
        max_val,max_time = self.MaxSize(size_file,after,before)
        
        min_val,min_time = self.MinSize(size_file,max_time,before)
        difference = max_val - min_val
        intermediate = float(max_val) - loss_fraction*difference
        rate = inf
        if not (max_val == -inf or min_val == inf):
            at_intermediate,intermediate_size = self.ReachesValAt(intermediate,
                                                                  size_file,1,max_time,
                                                                  before)
            rate = float(max_val - intermediate_size)/float(at_intermediate - max_time)
        return rate

##ToDo: generalize function to work with non-switching runs also
    def Mutations(self,_position,_before=None,_after=None,
                  _save_dir="global/",_mutation_file="mutations.dat"):
        '''Count mutations in a simulation interval.'''
        if _after is None:
            _after = 0
        if _before is None:
            _before = self.simulation_time
        file_path = os.path.join(self.name,_save_dir,_mutation_file)
        mutations = 0
        generation = 0
        if not os.path.exists(file_path):
            print "No file", _mutation_file, "found for", self.name
            mutations = float("nan")
        elif float(_before) > (_after + float(self.simulation_time - _after) /2.):
            for line in rev_readlines3(file_path):
                splits = line.split()
                try:
                    generation = int(splits[0])
                    if generation <= _before:
                        if generation < (_before-1):
                            print "no data between", generation, "and", _before
                            print "returning 'nan'"
                            mutations = float("nan")
                        else:
                            mutations = int(splits[_position-1])
                            break
            ##         else:
            ##            print "seen generation", generation
                except IndexError:
                    continue                    
        else:
            for line in open(file_path,'r'):            
                splits = line.split()
                generation = int(splits[0])
                if generation < _after:
                    continue
                if generation > _before:
                    break
                mutations = int(splits[_position-1])
            if generation < (_before-1):
                print "no data between", generation, "and", _before
                print "returning 'nan'"
                mutations = float("nan")
        return mutations

    def Wgds(self,save_dir,mutation_file="mutations.dat",before=None,after=None):
        return self.Mutations(8,_before=before,_after=after,
                              _mutation_file=mutation_file,
                              _save_dir=save_dir)

    def MutationList(self,_position,_before=None,_after=None,
                  _save_dir="global/",_mutation_file="mutations.dat"):
        '''Generate list of time_point-mutation tuples '''
        if _after is None:
            _after = 0
        if _before is None:
            _before = self.simulation_time
        file_path = os.path.join(self.name,_save_dir,_mutation_file)
        if not os.path.exists(file_path):
            print "No file", _mutation_file, "found for", self.name
            return None
        nr_mutations = 0
        mutationList = []
        for line in open(file_path,'r'):
            splits = line.split()
            generation = int(splits[0])
            if generation < _after:
                continue
            if generation > _before:
                break
            new_nr_mutations = int(splits[_position-1]) 
            mutation_step = new_nr_mutations - nr_mutations
            if mutation_step > 0:
                mutationList.append((generation,mutation_step))
                nr_mutations = new_nr_mutations
        return mutationList

    def WgdList(self,save_dir,mutation_file="mutations.dat",before=None,after=None):
        return self.MutationList(8,_before=before,_after=after,
                              _mutation_file=mutation_file,
                              _save_dir=save_dir)


    def ToSwitch(self):
        "check that run made it past environment switch time"
        if self.to_switch == False:
            for save in self.generation_saves:
                generation = Generation(save,self.generation_naming)
                if generation >= self.switch_time:
                    self.to_switch = True
        return self.to_switch

    def MutationRates(self, save_dir = "global_after/", size_file = "ancestor_sizes.dat",
                 mutation_file = "mutations.dat",_after = None,_before = None, steps = 10,
                 pos=1,relative = True):
        '''Rate of change from a particular mutation type, relative to
        the genome size. Change is calculated continuously in an
        interval, but then average rate over subintervals are returned'''
        rates = []
        if _after is None:
            _after = 0
        if _before is None:
            _before = self.simulation_time

        mutation_file_path = os.path.join(self.name,save_dir,mutation_file)
        size_file_path = os.path.join(self.name,save_dir,size_file)
        if not ( os.path.exists(mutation_file_path) or  os.path.exists(size_file_path) ):
            print "No file", mutation_file, "or", size_file, "found for", self.name
            return []

        changes = []
        size_previous = None
        mutations_previous = None
        for size_line,mut_line in zip(open(size_file_path),open(mutation_file_path) ):
            size_splits = size_line.split()
            mut_splits = mut_line.split()
            generation = int(size_splits[0])
            size = int(size_splits[1])
            mutations = int(mut_splits[pos])
            if size == 0:
                print "Found size 0 in", size_file_path, "at generation", generation
                print "Stopping further rate recording."
                break
            
            if mutations_previous is None or size_previous is None:
                '''Then it is the first line and we cannot calculate a rate'''
                mutations_previous = mutations
                size_previous = size
                continue
            change = 0
            if relative:
                change = (float(mutations) - mutations_previous)/ float(size_previous)
            else:
                change = mutations - mutations_previous
            changes.append((generation,change))
            mutations_previous = mutations
            size_previous = size
            if generation >= _before:
                break
            

        sum_change = 0
        prev_gen_step = _after
        stepsize = int((_before - _after) / steps)
        for i,(gen,change) in enumerate(changes,1):
            sum_change +=change
            if i % stepsize == 0:
                
                rates.append((gen, sum_change/float(gen - prev_gen_step)))
                sum_change = 0
                prev_gen_step = gen
                
        return rates
    def MutationRates2(self, save_dir = "global_after/", size_file = "ancestor_sizes.dat",
                 mutation_file = "mutations.dat",_after = None,_before = None, steps = 10,
                 relative = True):
        '''Rate of change from a particular mutation type, relative to
        the genome size. Change is calculated continuously in an
        interval, but then average rate over subintervals are returned'''
        rates = []
        if _after is None:
            _after = 0
        if _before is None:
            _before = self.simulation_time

        mutation_file_path = os.path.join(self.name,save_dir,mutation_file)
        size_file_path = os.path.join(self.name,save_dir,size_file)
        if not ( os.path.exists(mutation_file_path) or  os.path.exists(size_file_path) ):
            print "No file", mutation_file, "or", size_file, "found for", self.name
            return []

        changes = []
        size_previous = None
        mutations_previous = []
        for size_line,mut_line in zip(open(size_file_path),open(mutation_file_path) ):
            size_splits = size_line.split()
            mut_splits = mut_line.split()
            generation = int(size_splits[0])
            size = int(size_splits[1])
            mutations = map(int,mut_splits[1:])
            change = []
            if generation <_after:
                continue
            if size_previous is None:
                mutations_previous = mutations
                size_previous = size
                continue
            
            if size == 0:
                print "Found size 0 in", mutation_file_path, "at generation", generation
                print "Stopping further rate recording."
                break

            for i,mut in enumerate(mutations):
                

                '''Abolute change'''
                abs_change = mut - mutations_previous[i]                                
                '''Relative change'''
                rel_change = (float(mut) - mutations_previous[i])/ float(size_previous)

                abs_rel = (abs_change, rel_change)
                change.append(abs_rel)
                if generation >= _before:
                    break

            mutations_previous = mutations
            size_previous = size

            changes.append((generation,change))
            if generation >= _before:
                break

        sums_of_changes = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]] 
        prev_gen_step = _after
        stepsize = int((_before - _after) / steps)
        for i,(gen,change) in enumerate(changes,1):
            for j,mut_change in enumerate(change):
                sums_of_changes[j][0] += mut_change[0] # absolute change
                sums_of_changes[j][1] += mut_change[1] # relative change
                
            if i % stepsize == 0:
                past_generations = gen - prev_gen_step
                rates.append((gen, [ (x[0]/float(past_generations) , x[1]/float(past_generations))
                                                 for x in sums_of_changes] ))
                sums_of_changes = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
                prev_gen_step = gen
                
        return rates

    def Ohnologs(self,save_dir = "global_after/", ohnolog_file = "ohnologs.dat"):

        ohnolog_file_path = os.path.join(self.name,save_dir,ohnolog_file)
        if not os.path.exists(ohnolog_file_path):
            print "No file", ohnolog_file, "found for", self.name
            return []
        wgds = []
        for ohnolog_line in open(ohnolog_file_path):
            splits = ohnolog_line.split()
            generation = int(splits[0])
            ohnologs = [ (type_,int(d1),int(d2)) for type_,d1,d2 in [ tuple(re.split('[:,]',x)) for x in splits[1:] ] ]
            wgds.append((generation,ohnologs))
            
                
        return wgds

    def WGD_fitness_effects(self,save_dir = "global_after/", ohnolog_file = "ohnologs.dat"):
        ohnolog_file_path = os.path.join(self.name,save_dir,ohnolog_file)
        if not os.path.exists(ohnolog_file_path):
            print "No file", ohnolog_file, "found for", self.name
            return []
        wgds = []
        for ohnolog_line in open(ohnolog_file_path):
            splits = ohnolog_line.split()
            generation = int(splits[0])
            prev_fitness = self.ValAt("global_after/ancestor_standard_fitnesses.dat",1,generation)
            current_fitness = self.ValAt("global_after/ancestor_standard_fitnesses.dat",1,generation+1)
            wgds.append((generation,current_fitness-prev_fitness))
        return wgds
            
                


##     def GenomeDictionaries(self,save_dir=None,interval=2500,step_size=100,switch=True):

##         def dictify(_file):
##             d = dict()
##             for line in open(_file):
##                 splits = [ split.rstrip(":").strip("()") for split in line.split() ]
##                 ddict = dict({"type":splits[1]})
##                 ddict.update({ k:v for k,v in list(group3(splits[2:],2))  })
##                 d[int(splits[0])] = ddict
##             return d

##         after = ""
##         if switch:
##             after = "_after"
##         if save_dir is None:
##             save_dir = ("step_section_"+str(self.env_switch[1]+1)+
##             "_"+str(self.env_switch[1]+interval)+"_"+str(step_size)+after)
##         file_path=os.path.join(self.name,save_dir)
##         regex = "cell(\d+)\.str"
##         cell_regex = re.compile(regex)
##         cell_string_files = []
##         cell_string_files += [ match.group(0) for match
##                                in ( cell_regex.match(file_) for file_
##                                     in os.listdir(file_path)) if match ]

##         dicts = dict()
##         if not cell_string_files:
##             print "no cell string files found in", save_dir "for", self.name
##         for _file in cell_string_files:
##             m = cell_regex.search(_file)
##             cell_id = int(m.group(1))
##             dicts[cell_id] = dictify(os.path.join(file_path,_file))
##         return dicts



    def GenomeDictionaries(self,save_dir=None,interval=2500,step_size=100,switch=True,wgd_times=None):

        def dictify(_file):
            d = dict()
            for line in open(_file):
                splits = [ split.rstrip(":").strip("()") for split in line.split() ]
                ddict = dict({"type":splits[1]})
                ddict.update({ k:v for k,v in list(group3(splits[2:],2))  })
                d[int(splits[0])] = ddict
            return d

        generation_index = False
        if not save_dir:
            start = self.env_switch[1]+1
            stop = self.env_switch[1]+1 + interval
            step = step_size
            
            generations = range(start,stop,step)
            
            #generation_index = True
        elif wgd_times:
            generations = sorted([ gen for gen,n in wgd_times ])
            print "wgd times: ", generations
            #generation_index = True
        
        after = ""
        if switch:
            after = "_after"
        if save_dir is None:
            save_dir = ("step_section_"+str(self.env_switch[1]+1)+
            "_"+str(self.env_switch[1]+interval)+"_"+str(step_size)+after)
        file_path=os.path.join(self.name,save_dir)
        if not os.path.exists(file_path):
            print "path", file_path, "DOES NOT EXIST! SIMULATION WAS NEVER RUN"
            return None
        regex = "cell(\d+)\.str"
        cell_regex = re.compile(regex)
        cell_string_files = []
        cell_string_files += [ match.group(0) for match
                               in ( cell_regex.match(file_) for file_
                                    in os.listdir(file_path)) if match ]

        dicts = dict()
        if not len(cell_string_files):
            print "nothing found from", regex, "for", file_path

        cell_string_files.sort(key=lambda t: re.search('(\d+)',t).group(0))

        if generation_index:
            if len(generations) != len(cell_string_files):
                print ' generations and files do not match'

            for generation,_file in zip(generations,cell_string_files): #CL added zip
                m = cell_regex.search(_file)
                cell_id = int(m.group(1))
                print generation, cell_id,
                dicts[generation] = dictify(os.path.join(file_path,_file)) #CL cell_id -> generation
            print
        else:
            for _file in cell_string_files: 
                m = cell_regex.search(_file)
                cell_id = int(m.group(1))
                dicts[cell_id] = dictify(os.path.join(file_path,_file))
        return dicts
            

# changed to from x to *x (see https://github.com/prashmohan/GUPT/blob/master/gupt.py for example)
def spawn(f):  
        def fun(pipe,x):
            pipe.send(f(*x))  
            pipe.close()  
        return fun  

def parmap(f,X,cores):
        pipe=[Pipe() for x in X]
        processes = [Process(target=spawn(f),args=(c,x))
                     for x,(p,c) in izip(X,pipe)]
        def proc():
            for proc in processes:
                yield proc
        numProcesses = len(processes)
        processNum = 0  
        outputList = []

        free_cores = cores
        running = set()
        done = []
        process_gen = proc()
        print "starting", min(free_cores,numProcesses), "processes"
        while processNum < numProcesses:
            #print free_cores, "free cores"                        
            throw_out = set()
            for i in range(free_cores):
                try:
                    proc = process_gen.next()
                    time.sleep(0.5)
                except StopIteration:
                    break
                proc.start()
                free_cores -= 1
                print free_cores, "free cores after starting"
                running.add(proc)
                processNum +=1
            time.sleep(10)
            for proc in running:
                if not proc.is_alive():
                    free_cores += 1
                    print free_cores, "free cores after throwing out"                                        
                    throw_out.add(proc)                
                    done.append(proc)
            running -= throw_out

        for proc in done:  
            proc.join()  
        for proc,c in pipe:  
            outputList.append(proc.recv())
        return outputList
            
class NodeSet():
    '''Collection of runs at same node'''
    def __init__(self,loc,run_name,regex,simulation_time,
                 switch_time=None,switch_delay=0,
                 restore_dir_name=None,restore_regex=None):
        for X in self.__class__.__bases__:
            fun = getattr(X, '__init__', lambda x: None)
            fun(self)

        self.switch_time = switch_time
        self.switch_delay = switch_delay
        self.simulation_time = simulation_time
        self.regex = regex
        if restore_regex is None:            
            self.restore_regex = '$'
        else:
            self.restore_regex = restore_regex
        self.location = loc
        self.runs = self.CollectRuns(run_name, restore_dir_name)
    
    def CollectRuns(self,run_name,restore_dir_name=None,regex=None,restore_regex=None):
        if regex is None:
            regex = self.regex
        if restore_regex is None:
            restore_regex = self.restore_regex
        
        runs = []
        if os.path.exists(self.location.dir_):
            run_dirs = [os.path.join(self.location.dir_,x) for x in
                        os.listdir(self.location.dir_) if re.match(run_name+regex, x)]
            if not restore_dir_name:
                for run_dir in run_dirs:
                    runs.append((Run(run_dir,self.location.host,
                                     switch_time=self.switch_time,
                                     simulation_time=self.simulation_time,
                                     switch_delay=self.switch_delay)))
                    print run_dir
            else:
                for run_dir in run_dirs:
                    restore_dirs = [os.path.join(run_dir,x) for x in
                        os.listdir(run_dir) if re.match(restore_dir_name+restore_regex, x)]
                    for filename in restore_dirs: #os.listdir(run_dir):
#                        if restore_dir_name == filename:
                            load_dir = os.path.join(run_dir,filename)
                            runs.append((Run(load_dir,self.location.host,
                                             switch_time=self.switch_time,
                                             simulation_time=self.simulation_time,
                                             switch_delay=self.switch_delay,
                                             parent=run_dir)))
                            print load_dir
                            #break
                    
        return runs



    def RunIter(self):
        for run in self.runs:
            yield run

    def ApplyToSet(self,fun):
        po = Pool(4)
        for run in self.RunIter():
            po.apply_async(fun(run))
        po.close()
        po.join()

    def Exclude(self,indices,pos=-1):
        '''exclude a set of runs on their "index" '''
        new_nodeset=[]
        for run in self.runs:
            if run.index[pos] not in indices:
                new_nodeset.append(run)
            else:
                print "removing", run.index[pos]
        self.runs=new_nodeset
        return self.runs

    def Include(self,indices,pos=-1):
        '''include a set of runs on their "index" '''
        new_nodeset=[]
        for run in self.runs:
            if run.index[pos] in indices:
                print "including", run.index[pos]
                new_nodeset.append(run)
        self.runs=new_nodeset
        return self.runs

    def free_cores(self):
        host = self.location.host
        cores = subprocess.check_output(["ssh",host,"grep -c processor /proc/cpuinfo"])
        top_out = subprocess.check_output(["ssh",host,"top","-b -n 1"])
        long_load = math.ceil(float(top_out.split('\n')[0].split()[-1])-0.2)
        print host
        return int(max(1,(int(cores) - long_load)))

    


class Batch():
    '''Collection of runs with a common name'''
    name = ""
    @interactive
    def __init__(self,run_id,regex,simulation_time,
                 switch_time=None,switch_delay=0,save_dir=None,
                 restore_dir_name=None,restore_regex=None):
        for X in self.__class__.__bases__:
            fun = getattr(X, '__init__', lambda x: None)
            fun(self)
        self.run_id = run_id
        if restore_dir_name:
            self.name = run_id+regex+restore_dir_name
        else:
            self.name = run_id
        if save_dir is None:
            save_dir = os.path.join(local_saves,self.name)
        self.save_dir = save_dir
        self.regex = regex
        self.restore_regex = restore_regex
        self.nodesets=[]
        self.simulation_time = simulation_time
        self.switch_time=switch_time
        self.switch_delay=switch_delay
        self.restore_dir_name=restore_dir_name
        
    def SayName(self):
        print self.name

    def Size(self):
        return len( [run for run in self.RunIter() ] )

    def CollectRuns(self,nodes,regex=None):
        if regex is None:
            regex = self.regex
        for node in nodes:
            nodeset = NodeSet(node,self.run_id,regex,
                              self.simulation_time,
                              self.switch_time,
                              self.switch_delay,
                              self.restore_dir_name,
                              self.restore_regex)
            if nodeset.runs:
                self.nodesets.append(nodeset)

    def Exclude(self,indices,pos=-1):
        for nodeset in self.SetIter():
            nodeset.Exclude(indices,pos)

    def Include(self,indices,pos=-1):
        for nodeset in self.SetIter():
            nodeset.Include(indices,pos)

    def RunIter(self):
        for nodeset in self.SetIter():
            for run in nodeset.RunIter():
                yield run

    def SetIter(self):
        for nodeset in self.nodesets:
            yield nodeset
                
    def free_cores(self):

        return 4

    def GenerationSaves(self,regex="^generation(\d)+S?$"):
        
        for set in self.SetIter():
            set.ApplyToSet(lambda x: Run.GenerationSaves(x,regex))

    def Ordered(self,value_func=lambda y:Run.AverageFitness(y,_after=-1000,_before=10000)):
        '''return an ordered list of runs (e.g. ordered on final fitness'''
        return sorted(self.RunIter(),key=lambda x:value_func(x))
                        
    def FitUnfit(self,after=-1000,before=10000,_threshold=0.85):
        '''return a tuple of lists of files '''
        fit=[]
        unfit=[]
        value_func=lambda y:Run.AverageFitness(y,_after=after,_before=before,threshold=_threshold)
        for (fitness,run) in [ (value_func(x),x) for x in self.RunIter() ]:
            if fitness < threshold:
                unfit.append((fitness,run))
            else:
                fit.append((fitness,run))
        fit.sort()
        unfit.sort()
        return ([ run for (fitness,run) in fit],[ run for (fitness,run) in unfit])


    def CollectEnvSwitchFiles(self,file_id,separate=False,local_dir=None):
        if local_dir is None:
            local_dir = local_saves+self.name
        if not os.path.exists(local_dir):
            print 'no store directory\ncreating new dir',local_dir
            os.makedirs(local_dir)
        base_name = os.path.basename(file_id)
        splits = base_name.rsplit('.')
        extension = ""
        if not len(splits) == 1:
            base_name = splits[0]
            extension = splits[1]
        for run in self.RunIter():
            before_data_file = os.path.join(run.name,"global_before",file_id)
            after_data_file = os.path.join(run.name,"global_after",file_id)
            if os.path.exists(before_data_file) and os.path.exists(after_data_file):
                if not self.restore_dir_name:
                    run_id = os.path.basename(os.path.normpath(run.name))
                else:
                    head, tail = os.path.split(os.path.normpath(run.name))
                    run_id = os.path.split(head)[1]+'_'+tail
                out = open(os.path.join(local_dir,base_name)+"."+run_id+"."+extension,'w')
                print >> out, open(before_data_file).read() ,
                if separate:
                    print >> out, '\n',
                print >> out, open(after_data_file).read()
#                content = open(before_data_file).read() + separator + open(after_data_file).read()
            else:
                print file_id, "not found in", run.name        
    
    def CollectFiles(self,file_id,local_dir=None):
        '''copy files to a local directory, adding run identifiers to filename '''
        if local_dir is None:
            local_dir = local_saves+self.name
        if not os.path.exists(local_dir):
            print 'no store directory\ncreating new dir',local_dir
            os.makedirs(local_dir)
        base_name = os.path.basename(file_id)
        splits = base_name.rsplit('.')
        extension = ""
        if not len(splits) == 1:
            base_name = splits[0]
            extension = splits[1]
        for run in self.RunIter():
            data_file = os.path.join(run.name,file_id)
            if not self.restore_dir_name:
                run_id = os.path.basename(os.path.normpath(run.name))
            else:
                head, tail = os.path.split(os.path.normpath(run.name))
                run_id = os.path.split(head)[1]+'_'+tail                                           
            if(os.path.exists(data_file)):
                shutil.copy(data_file,os.path.join(local_dir,base_name)+"."+run_id+"."+extension)
            else:
                print file_id, "not found in", run.name

    def Remove(self,remove=False):
        '''Remove the directories and log files associated with this simulation'''
        def remove_it(run):
            if remove:
                print "REMOVING", run.name,"....",
                retcode = subprocess.call(["ssh",run.host,"rm -rf",run.name])
                retcode = subprocess.call(["ssh",run.host,"rm -rf",run.name+".*"])
                print "done"
            else:
                print "I would have liked to remove", run.name            
        
        set_job = lambda s: parmap(lambda x: remove_it(x),[(r,) for r in NodeSet.RunIter(s)],
                                   min(2,NodeSet.free_cores(s)))
        parmap(set_job,[(s,) for s in self.nodesets] ,len(self.nodesets))

    def RunOnHosts(self,hosts,func):
        runs = self.RunIter()
        host_jobs=[]
        for _host in hosts:
            free = free_cores(_host)
            host_runs = []
            for i in range(free):
                try:
                    host_runs.append(runs.next())
                except StopIteration:
                    break
            host_bjobs.append(parmap(lambda x: func(x,h=_host),host_runs,free))
        parmap(lambda x: x,host_jobs,len(hosts))

    def RunSequentialOnHosts(self,hosts,job,job_args_list):#env_files,systematic_names):
        '''As RunOnHosts, but runs a predefined set of jobs on host,
        starting new ones as cores become free'''

        job_args_dict={}

        weighed_hosts=[]
        for host in hosts:
            for i in range(free_cores(host)):
                weighed_hosts.append(host)
        shuffle(weighed_hosts)
    
        host_cycle = cycle(weighed_hosts)
        for job_args in job_args_list:
            '''go through different jobs /simulation types'''            
            
            for run in self.RunIter():
                _host = host_cycle.next()
                args = (run,)+job_args+(_host,)
                if job_args_dict.has_key(_host):
                    job_args_dict[_host].append(args)
                else :
                    job_args_dict[_host] = [args]

        processes = []
        pipes = []
        for key in job_args_dict:
            free = free_cores(key)
            host=key
#            for  run,env,name,host in job_args_dict[key]:
#                print "appending job with args",run.name,env,name,host
            print "to list for",key
            fun = lambda f: parmap(job,job_args_dict[key],f)
            pipe = Pipe()
            pipes.append(pipe)
            p,c = pipe
            process = Process(target=spawn(fun),args=(c,(free,)))
            processes.append(process)
            process.start()

        outputList = []
        for proc in processes:
            proc.join()
        for proc,c in pipes:
            outputList.append(proc.recv())
        return outputList

def free_cores(host):
    cores = subprocess.check_output(["ssh",host,"grep -c processor /proc/cpuinfo"])
    top_out = subprocess.check_output(["ssh",host,"top","-b -n 1"])
    long_load = math.ceil(float(top_out.split('\n')[0].split(":")[-1].split(", ")[0])-0.2)
    print host, "load", str(long_load)
    return int(max(1,(int(cores) - long_load)))

def Options():
    def split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))


    parser = OptionParser()
    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose", default=1,
                      help="don't print status messages to stdout")
    parser.add_option("-L", "--locations",
                      action="append", type="string", dest="locations",
                      help="add a new host node to search runs")
    parser.add_option( "--mutant-hosts",
                      action="callback", type="string", callback=split_callback,dest="mutants",
                      help="choose your mutants by number")
    parser.add_option("-s", "--start-at",
                      action="store", type="int", dest="start", default=1,
                      help="start mutant")
    parser.add_option("-e", "--end-at",
                      action="store", type="int", dest="end", default=41,
                      help="end mutant")
    parser.add_option("-k", "--knockout",
                      action="store_true", dest="knockout", default=False,
                      help="knockout analyze")
    parser.add_option("-n", "--name",
                      action="store", type="string",dest="name", default="wgd_change_to_envV_",
                      help="run identifier")
    parser.add_option("--restore-dir-name",
                      action="store", type="string",dest="restore_dir", 
                      help="restore directory identifier")
    parser.add_option("-r", "--regex",
                      action="store", type="string",dest="regex", default="(\d)+$",
                      help="regular expression to search runs")
    parser.add_option("--restore-regex",
                      action="store", type="string",dest="restore_regex", default=None,
                      help="regular expression to search restore runs")
    
    parser.add_option("-c", "--env-config",
                      action="store", type="string",dest="env_config",
                      help="environment configuration file")
    parser.add_option("--simulation-time",
                      action="store",type="int",dest="simulation_time",
                      help="set the simulation time for this run")
    parser.add_option("--switch-time",
                      action="store",type="int",dest="switch_time", 
                      help="set the switch time for this run")
    parser.add_option("--switch-delay",
                      action="store",type="int",dest="switch_delay", 
                      help="set the switch time for this run")
    parser.add_option("-E","--exclude",
                      action="store",type="string",dest="excluded", 
                      help="exclude runs")
    parser.add_option("--include",
                      action="callback",type="string",callback=split_callback,dest="included", 
                      help="include runs")
    parser.add_option("-l", "--continue-runs",
                      action="store_true",dest="continue_runs", default=False,
                      help="continue the runs from a save file")
    parser.add_option("--overwrite",
                      action="store_true",dest="overwrite",default=False,
                      help="overwrite existing restore directories")


    return parser
    
def main(argv=None):
    '''for testing purposes. For dedicated tasks, import the
    batch_tools in a separate python script (see for example
    wgd_analysis.batch.py'''
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    (options, args) = parser.parse_args()
    
    paths=[]
    batch = Batch("tfb_dds_pme_fitness_landscape_long_",'(\d)+$')
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    
    batch.CollectRuns(paths)

    for set in batch.SetIter():
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))

    #batch.Exclude([28,29,16])
    ordered = batch.Ordered()
    print
    fit,unfit = batch.FitUnfit()
    print "fits:",
    for run in fit:
        print run.name,
    print
    print "unfits:",
    for run in unfit:
        print run.name,

    print
    for run in unfit:
        #print run.GraceSave()
        print run.MaxSize(_after=0,_before=1000), run.MinSize(_after=1000,_before=10000)
    print
    for run in fit:
        #print run.GraceSave()
        print run.MaxSize(_after=0,_before=1000), run.MinSize(_after=1000,_before=10000)

    #batch.CollectFiles("graceplot.agr")

if __name__ == "__main__":
    sys.exit(main())


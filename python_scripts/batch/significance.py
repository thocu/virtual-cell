##! /usr/bin/python
from batch_tools import *
import random, scipy
from itertools import *
from scipy import stats


fitness_boundaries = [0.95,1.05] # boundaries define the cutoff point for counting mutations
generation_boundaries = [100,200,400]#[100,200,400] # define the cohorts to sample


def fold_size_increase_early(run,before=1000):
    start_size = run.MaxSize(_before=1)
    max_size = run.MaxSize(_before=before)
    return float(max_size)/float(start_size)

def fold_size_reduction_late(run,partition=1000):
    max_size = run.MaxSize(_before=partition)
    min_size = run.MinSize(_after=partition)
    return float(max_size)/float(min_size)

def size_increase_early(run,before=1000):
    start_size = run.MaxSize(_before=1)
    max_size = run.MaxSize(_before=before)
    return float(max_size) - float(start_size)

def size_reduction_late(run,partition=1000):
    max_size = run.MaxSize(_before=partition)
    min_size = run.MinSize(_after=partition)
    return float(max_size) - float(min_size)

def order(x):
    """ Returns the order of each element in x as a list. """
    L = len(x)
    rangeL = range(L)
    z = izip(x, rangeL)
    z = izip(z, rangeL) #avoid problems with duplicates.
    D = sorted(z)
    return [d[1] for d in D]

def rank(x):
    """ Returns the rankings of elements in x as a list. """
    L = len(x)
    ordering = order(x)
    ranks = [0] * len(x)
    for i in range(L):
        ranks[ordering[i]] = i
    return ranks

def make_data(run,data_file,columns,generation_col=0):
    '''Read in a data file as numpy array.'''
    file_path = os.path.join(run.name,data_file)
    data=np.empty( (0,2) )
    if os.path.exists(file_path):
        for col in columns:
            row = np.loadtxt(file_path,usecols = [generation_col,col])
            data = np.vstack((data,row))
    else:
        print data_file, "not found for", run.name
    return data    

def combine_data(runs,data_file,columns,generation_col=0):
    '''Combine multiple numpy arrays from data files to 1 array.'''
    combined=np.empty( (0,2) ) # array with data to return
    for run in runs:
        data = make_data(run,data_file,columns,generation_col=0)
        combined = np.vstack((combined,data))
    return combined
    
def make_samples(set1,set2,n):
    '''Create a collection of new data sets, by randomizing original sets.'''
    shuffles=[]
    for i in range(n):
        as_sample1=[]
        as_sample2=[]
        shuffled=set1+set2
        random.shuffle(shuffled)
        shuffles.append((shuffled[:len(set1)],shuffled[len(set1):]))
    return shuffles

def rank_sums_cohorts(data_set1,data_set2):
    ''''''
    as_rank_sums1=[]
    as_rank_sums2=[]
    for i in range(len(data_set1[0])):
        data=[ cohorts[i] for cohorts in data_set1+data_set2 ]
        ranks=rank(data)
        as_rank_sums1.append(sum(ranks[:len(data_set1)]))
        as_rank_sums2.append(sum(ranks[len(data_set1):]))
    return as_rank_sums1,as_rank_sums2

def rank_sums_partitions(data_set1,data_set2):
    as_rank_sums1=[]
    as_rank_sums2=[]
    for i in range(len(data_set1[0])):
        as_cohort_rank_sums1=[]
        as_cohort_rank_sums2=[]
        stacked_cohort=[ cohorts[i] for cohorts in data_set1+data_set2 ]
        for j in range(len(data_set1[0][0])):
            data= [ fractions[j] for fractions in stacked_cohort ]
            ranks=rank(data)
            as_cohort_rank_sums1.append(sum(ranks[:len(data_set1)]))
            as_cohort_rank_sums2.append(sum(ranks[len(data_set1):]))
        as_rank_sums1.append(as_cohort_rank_sums1)
        as_rank_sums2.append(as_cohort_rank_sums2)
    return as_rank_sums1,as_rank_sums2
                             
            
def average_fraction(data_set):
    average_fraction=[ [ 0. for i in range(len(data_set[0][0]))]
                       for j in range(len(data_set[0])) ]     
    for data in data_set:
        for i, cohort in enumerate(data):
            for j, partition in enumerate(cohort):
                average_fraction[i][j]+=partition/float(len(data_set))
    return average_fraction

def average_cohort(data_set):
    average_cohort=[ 0. for j in range(len(data_set[0])) ]
    for data in data_set:
        for i,cohort in enumerate(data):
            average_cohort[i]+=cohort/float(len(data_set))
    return average_cohort
        
def to_cohorts(data,delims):
    '''Partition data points in a numpy array data structure by
    delimiting on the 1st axis (generation)'''
    parts=[[] for i in range(len(delims)+1)]
    for point in data:
        for y,delim in enumerate(delims):            
            if point[0] <= delim:
                parts[y].append([point[0],point[1]])
                break
        else:
            parts[-1].append([point[0],point[1]]) # else put it in the 'last' cohort
    return parts
    
def partition_cohorts(cohorts,partition_bounds):
    ''''''
    cohorts_partitioned=[]
    for cohort in cohorts:
        partitions=[[] for x in range(len(partition_bounds)+1)]
        for point in cohort:
            for y,bound in enumerate(partition_bounds):
                if point[1] < bound:
                    partitions[y].append(point)
                    break
            else :
                partitions[-1].append(point)
        cohorts_partitioned.append(partitions)
    return cohorts_partitioned

def cohort_averages(cohorts):
    return [ reduce(lambda l,m: l+m[1],cohort,0.)/float(len(cohort)) for cohort in cohorts ]

def partition_fractions(partitioned_cohorts):
    return [ map(lambda y: float(y)/float(reduce(lambda l,m:l+len(m),d,0)),
                 (map(lambda x:len(x),d))) for d in partitioned_cohorts ]

def sample_test_2d(data_file,sample1,sample2,cohort_boundaries,
                   fitness_boundaries,n,columns,out_file=open("/dev/null",'w')):
    '''make tuples with runs as keys'''
    data_dict = dict([ (run.name,partition_fractions(
        partition_cohorts(to_cohorts(make_data(
        run,data_file,columns),cohort_boundaries),fitness_boundaries)))
                       for run in sample1+sample2 ])
    '''fit runs'''
    sample1_singles_fractions = [ data_dict[run.name] for run in sample1 ]
    sample2_singles_fractions = [ data_dict[run.name] for run in sample2 ]
    sample1_fractions=average_fraction(sample1_singles_fractions)
    sample1_fraction_matrix=np.transpose(sample1_fractions)
    sample2_fractions=average_fraction(sample2_singles_fractions)
    sample2_fraction_matrix=np.transpose(sample2_fractions)

    (sample1_rank_sums,sample2_rank_sums)=rank_sums_partitions(sample1_singles_fractions,
                                                               sample2_singles_fractions)
    sample1_rank_sums_matrix=np.transpose(sample1_rank_sums)
    sample2_rank_sums_matrix=np.transpose(sample2_rank_sums)
#    print "sets_rank_sums", sample1_rank_sums,sample2_rank_sums
    '''sampling'''
    as_set1_as_set2_pairs=make_samples(sample1,sample2,n)

    '''alternative sampling '''
    alternative_samplings1_partition_fractions=[]
    alternative_samplings2_partition_fractions=[]
    alternative_samplings1_partition_rank_sums=[]
    alternative_samplings2_partition_rank_sums=[]

    for as_set1,as_set2 in as_set1_as_set2_pairs:
        as_set1_data= [ data_dict[run.name] for run in as_set1 ]
        alternative_samplings1_partition_fractions.append(average_fraction(as_set1_data))
        as_set2_data= [ data_dict[run.name] for run in as_set2 ]
        alternative_samplings2_partition_fractions.append(average_fraction(as_set2_data))

        (rank_sums1,rank_sums2)=rank_sums_partitions(as_set1_data,as_set2_data)
        alternative_samplings1_partition_rank_sums.append(rank_sums1)
        alternative_samplings2_partition_rank_sums.append(rank_sums2)
        
        
    alternative_samplings1_matrix=np.transpose(alternative_samplings1_partition_fractions)
    alternative_samplings2_matrix=np.transpose(alternative_samplings2_partition_fractions)

    rank_sums_samplings1_partition_matrix=np.transpose(alternative_samplings1_partition_rank_sums)
    rank_sums_samplings2_partition_matrix=np.transpose(alternative_samplings2_partition_rank_sums)


    trans1=np.transpose(sample1_singles_fractions,(1,2,0))
    trans2=np.transpose(sample2_singles_fractions,(1,2,0))

    differences1=trans1[:,0,:]-trans1[:,2,:]
    differences2=trans2[:,0,:]-trans2[:,2,:]
    print differences1
    for i,(cohort1,cohort2) in enumerate(zip(differences1,differences2)):
        print "cohort", i
        print stats.mannwhitneyu(cohort1,cohort2)

    
    print "Mann-Whitney-U scores"
    for i,(cohort1,cohort2) in enumerate(zip(trans1,trans2)):
        print "cohort", i
        for j,(fraction1,fraction2) in enumerate(zip(cohort1,cohort2)):
            print "partition", j,
            print stats.mannwhitneyu(fraction1,fraction2)

    print
    print "label samplings", "average compare"
    print "\tpositives"
    for partition_a, partition_s in zip(sample1_fraction_matrix,alternative_samplings1_matrix):
        for val,cohort in zip(partition_a,partition_s):
            raw_score=reduce(lambda p,q:p+int(q),map(lambda y:val<=y,cohort))
            P=float(raw_score)/float(n)
            print P,
        print
    print "\tnegatives"
    for partition_a, partition_s in zip(sample2_fraction_matrix,alternative_samplings2_matrix):
        for val,cohort in zip(partition_a,partition_s):
            raw_score= reduce(lambda p,q:p+int(q),map(lambda y:val<y,cohort))
            P=float(raw_score)/float(n)
            print P,
        print

    print
    print "label samplings","rank compare"
    print "\tpositives"
    for i,(partition_a, partition_s) in enumerate(zip(sample1_rank_sums_matrix,
                                                      rank_sums_samplings1_partition_matrix)):
        print >>out_file,i,
        for j,(val,cohort) in enumerate(zip(partition_a,partition_s)):
            raw_score=reduce(lambda p,q:p+int(q),map(lambda y:val<=y,cohort))
            P=float(raw_score)/float(n)
            print >> out_file, P,
            print P,
        print
        print >> out_file
    print >> out_file
    print "\tnegatives"
    for partition_a, partition_s in zip(sample2_rank_sums_matrix,
                                        rank_sums_samplings2_partition_matrix):
        for val,cohort in zip(partition_a,partition_s):
            raw_score=reduce(lambda p,q:p+int(q),map(lambda y:val<y,cohort))
            P=float(raw_score)/float(n)
            print P,
        print

    print
def sample_test(data_file,sample1,sample2,cohort_boundaries,n,
                columns,out_file=open("/dev/null",'w')):

    data_dict= dict([ (run.name,cohort_averages(
        to_cohorts(make_data(run,data_file,columns),
                   cohort_boundaries))) for run in sample1+sample2 ])
    '''fit runs'''
    sample1_singles_cohorts = [ data_dict[run.name] for run in sample1 ]
    sample2_singles_cohorts = [ data_dict[run.name] for run in sample2 ]
    sample1_cohorted_averages=average_cohort(sample1_singles_cohorts)
    sample2_cohorted_averages=average_cohort(sample2_singles_cohorts)

    (sample1_rank_sums,sample2_rank_sums)=rank_sums_cohorts(
        sample1_singles_cohorts,sample2_singles_cohorts)
#    print "sets rank sums", sample1_rank_sums, sample2_rank_sums
    as_set1_as_set2_pairs=make_samples(sample1,sample2,n)

    '''alternative sampling'''
    alternative_samplings1=[]
    alternative_samplings2=[]

    alternative_samplings1_rank_sums=[]
    alternative_samplings2_rank_sums=[]
    for as_set1,as_set2 in as_set1_as_set2_pairs:
        as_set1_data= [data_dict[run.name] for run in as_set1 ]
        alternative_samplings1.append(average_cohort(as_set1_data))
        as_set2_data= [data_dict[run.name] for run in as_set2 ]
        alternative_samplings2.append(average_cohort(as_set2_data))

        (rank_sums1,rank_sums2)=rank_sums_cohorts(as_set1_data,as_set2_data)
        alternative_samplings1_rank_sums.append(rank_sums1)
        alternative_samplings2_rank_sums.append(rank_sums2)
#        print rank_sums1, rank_sums2
        
    alternative_samplings1_matrix=np.transpose(alternative_samplings1)
    alternative_samplings2_matrix=np.transpose(alternative_samplings2)

    rank_sums_samplings1_matrix=np.transpose(alternative_samplings1_rank_sums)
    rank_sums_samplings2_matrix=np.transpose(alternative_samplings2_rank_sums)

    trans1=np.transpose(sample1_singles_cohorts)
    trans2=np.transpose(sample2_singles_cohorts)

    print "Mann-Whitney-U scores"
    for i,(cohort1,cohort2) in enumerate(zip(trans1,trans2)):
        print "cohort", i,
        print stats.mannwhitneyu(cohort1,cohort2)

    print "label samplings", "average compare"
    print "\tpositives"
    for average, cohort_s in zip(sample1_cohorted_averages,alternative_samplings1_matrix):
        raw_score= reduce(lambda p,q: p+int(q),map(lambda y:average <=y,cohort_s))
        P=float(raw_score)/float(n)
        print >>out_file, P
        print P
        
    print "\tnegatives"
    for average, cohort_s in zip(sample2_cohorted_averages,alternative_samplings2_matrix):
        raw_score=reduce(lambda p,q: p+int(q),map(lambda y:average <y,cohort_s))
        P=float(raw_score)/float(n)
        print P        
    print >> out_file
    print "label samplings","rank compare"
    print "\tpostives"
    for rank_sum, cohort_s in zip(sample1_rank_sums,rank_sums_samplings1_matrix):
        raw_score=reduce(lambda p,q: p+int(q),map(lambda y:rank_sum <=y,cohort_s))
        P=float(raw_score)/float(n)
        print >>out_file, P
        print P        

    print "\tnegatives"
    for rank_sum, cohort_s in zip(sample2_rank_sums,rank_sums_samplings2_matrix):
        raw_score=reduce(lambda p,q: p+int(q),map(lambda y:rank_sum <y,cohort_s))
        P=float(raw_score)/float(n)
        print P
    print >> out_file
    print


def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end

    for i in range(options.start,options.end):
        paths.append(Location.fromhost("mutant"+str(i)))

    #    for location in options.locations:
    location = "/home/thocu/thocu2/mutant6_2011_08_11/"
    paths.append(Location(location))
    
    batch = Batch('tfb_dds_pme_fitness_landscape_long_','(\d)+$',10000)
    batch.SayName()
    batch.CollectRuns(paths)
    batch.Exclude([313,326,354,355,363,371])
    ids = set([ run.index for run in batch.RunIter()])
    
    (fit_long,unfit_long) = batch.FitUnfit()
    fit_ids = [run.index for run in fit_long]
    unfit_ids = [run.index for run in unfit_long]

    landscape_batch = Batch("tfb_dds_pme_fitness_landscape_",'(\d)+$',1000)
    landscape_batch.CollectRuns(paths)
    fit_sample = [run for run in landscape_batch.RunIter() if run.index in fit_ids]
    unfit_sample = [run for run in landscape_batch.RunIter() if run.index in unfit_ids]

    fit_short_ids = set([ run.index for run in fit_sample])
    print "fits:", sorted(fit_short_ids)
    unfit_short_ids = set([ run.index for run in unfit_sample])
    print "unfits:", sorted(unfit_short_ids)
    print ids -  fit_short_ids - unfit_short_ids
    
    size=open('significance_size.dat','w')
    fitness_landscape_dup=open('significance_fitness_landscape_dup.dat','w')
    fitness_landscape_del=open('significance_fitness_landscape_del.dat','w')


    print "sample_sizes", len(fit_sample), len(unfit_sample)
    print "size"
    sample_test('step_section_1_1000_20/ancestor_sizes.dat',
                fit_sample,unfit_sample,generation_boundaries,100,[1],size)
    print "standard fitness"
    sample_test('step_section_1_1000_20/ancestor_standard_fitnesses.dat',
                fit_sample,unfit_sample,generation_boundaries,100,[1])
    print "life time fitness"
    sample_test('step_section_1_1000_20/ancestor_real_fitnesses.dat',
                fit_sample,unfit_sample,generation_boundaries,100,[1])
    print 'fitness landscape'
    print 'duplications'
    sample_test_2d('step_section_1_1000_20/major_mutation_robustness.dat',
                   fit_sample,unfit_sample,generation_boundaries,fitness_boundaries,
                   100,range(1,51),fitness_landscape_dup)
    print 'deletions'
    sample_test_2d('step_section_1_1000_20/major_mutation_robustness.dat',
                   fit_sample,unfit_sample,generation_boundaries,fitness_boundaries,
                   100,range(51,101),fitness_landscape_del)

    fit_fold_increases =  [fold_size_increase_early(run) for run in fit_long ]
    unfit_fold_increases = [fold_size_increase_early(run) for run in unfit_long ]

    print "fits average fold increase early (avrg,std):", scipy.mean(fit_fold_increases), scipy.std(fit_fold_increases)
    print "unfits average fold increase early (avrg,std):", scipy.mean(unfit_fold_increases), scipy.std(unfit_fold_increases)
    print
    
    fit_fold_reductions =  [fold_size_reduction_late(run) for run in fit_long ]
    unfit_fold_reductions = [fold_size_reduction_late(run) for run in unfit_long ]

    print "fits average fold decrease late (avrg,std):",scipy.mean(fit_fold_reductions), scipy.std(fit_fold_reductions)
    print "unfits average fold decrease late (avrg,std):",scipy.mean(unfit_fold_reductions), scipy.std(unfit_fold_reductions)
    print stats.mannwhitneyu(fit_fold_reductions,unfit_fold_reductions)
    
    fit_size_increases = [size_increase_early(run) for run in fit_long ]
    unfit_size_increases = [size_increase_early(run) for run in unfit_long ]
    print "fit size increase (avrg, std):", scipy.mean(fit_size_increases),scipy.std(fit_size_increases)
    print "unfit increase (avrg, std):", scipy.mean(unfit_size_increases),scipy.std(unfit_size_increases)

    fit_size_reductions = [size_reduction_late(run) for run in fit_long ]
    unfit_size_reductions = [size_reduction_late(run) for run in unfit_long ]
    print "fit size decrease (avrg,std):", scipy.mean(fit_size_reductions),scipy.std(fit_size_reductions)
    print "unfit size decrease (avrg,std):", scipy.mean(unfit_size_reductions),scipy.std(unfit_size_reductions)

    fit_init_vals = [ run.MaxSize(_before=1) for run in fit_long ]
    unfit_init_vals = [ run.MaxSize(_before=1) for run in unfit_long ]
    print "fit initial sizes (avrg,std):", scipy.mean(fit_init_vals),scipy.std(fit_init_vals)
    print "unfit initial sizes (avrg,std):", scipy.mean(unfit_init_vals),scipy.std(unfit_init_vals)


    print "overall average fold increase early (avrg,std):", scipy.mean(fit_fold_increases + unfit_fold_increases), scipy.std(fit_fold_increases + unfit_fold_increases)
    print "overall average fold decrease late (avrg,std):", scipy.mean(fit_fold_reductions + unfit_fold_reductions), scipy.std(fit_fold_reductions + unfit_fold_reductions)



if __name__ == "__main__":
    sys.exit(main())



from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    parser.add_option("-C", "--collect",
                      action="store", type="string",dest="collect",
                      help="search for file name identifier and collect in folder")
    parser.add_option("--env-change",
                      action="store_true",dest="env_change",default=False,
                      help="set data are separated into a before and after directory")
    parser.add_option("--separator",
                      action="store_true",dest="separator",default=False,
                      help="character to indicate joining point of files")
    
    (options, args) = parser.parse_args()
    
    paths=[]
    batch = Batch(options.name,options.regex,options.simulation_time,
                  restore_dir_name=options.restore_dir)
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    
    batch.CollectRuns(paths)
##     ordered = batch.Ordered()
##     print
##     fit,unfit = batch.FitUnfit()
##     print "fits:",
##     for run in fit:
##         print run.name,
##     print
##     print "unfits:",
##     for run in unfit:
##         print run.name,

    if options.env_change:
        batch.CollectEnvSwitchFiles(options.collect,options.separator)
    else:
        batch.CollectFiles(options.collect)

if __name__ == "__main__":
    sys.exit(main())


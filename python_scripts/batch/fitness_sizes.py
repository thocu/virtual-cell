from batch_tools import *

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    parser = Options()
    (options, args) = parser.parse_args()
    paths = []
    print "options", options.start, options.end
    if not options.locations:
        for i in range(options.start,options.end):
            paths.append(Location.fromhost("mutant"+str(i)))
    else:
        for location in options.locations:
            paths.append(Location(location))
    
    batch = Batch(options.name,"(\d)+$",options.simulation_time)
    batch.SayName()
    batch.CollectRuns(paths)
        
    for set in batch.SetIter():
        print set.location.dir_
        set.ApplyToSet(lambda x: Run.GenerationSaves(x))
        set.ApplyToSet(lambda x: Run.FitBefore(x))
        set.ApplyToSet(lambda x: Run.Finished(x))

    for x in batch.RunIter():
        if not x.finished:
            print x.name, "did not finish yet"
    
    fits, unfits = batch.FitUnfit(before=options.simulation_time)

    print len(fits), "fits and", len(unfits), "unfits"
if __name__ == "__main__":
    sys.exit(main())





import sys, os, re, subprocess
import math
def main(argv=None):
    if argv is None:
        argv = sys.argv

    mut_file_id="mutations"
    size_file_id="ancestor_sizes"
    rename= "wgds"
    regex = '.*_(\d)+\.dat'
    location = '/home/thocu/WB/python_scripts/local_files/wgd_change_to_envVII_small_gcr_delay1000_long_/'
        
    mut_files = [os.path.join(location,x) for x in os.listdir(location) if re.match(mut_file_id+regex, x) ]
    size_files = [os.path.join(location,x) for x in os.listdir(location) if re.match(size_file_id+regex, x) ]
    mut_files.sort()
    size_files.sort()

    catch_id = re.compile('.*_(\d+)\.dat')
    if True:
        for mut_file,size_file in zip(mut_files,size_files):
            mutations = open(mut_file)
            sizes = open(size_file)
            previous = 0
            id_match = catch_id.match(mut_file)
            nr = id_match.group(1)
            out = open(os.path.join(location,rename+"_"+nr+".dat"),'w')
            for line in mutations:
                if not line.strip():
                    previous = 0
                    continue
                generation,mut = int(line.split()[0]),int(line.split()[7])
                if mut > previous:
                    mutations = mut - previous
                    previous = mut
                    while True:
                        line2 = sizes.readline()
                        generation2,size = int(line2.split()[0]),int(line2.split()[1])
                        if generation2 == generation:                        
                            print >> out , generation2, size
                            break


    fitness_file_id="ancestor_standard_fitness"
    wgd_files = [os.path.join(location,x) for x in os.listdir(location) if re.match(rename+regex, x) ]
    fitness_files = [os.path.join(location,x) for x in os.listdir(location) if re.match(fitness_file_id+regex, x) ]
    wgd_files.sort()
    fitness_files.sort()

    param = "/home/thocu/WB/grace_param_files/for_wgd_overview.par"
    batch = "/home/thocu/WB/grace_param_files/for_overview_print.bfile"
    for fitness_file,size_file,wgd_file in zip(fitness_files,size_files,wgd_files):
        id_match = catch_id.match(fitness_file)
        nr = id_match.group(1)
        printfile = os.path.join(location,"overview_"+nr+".png")
        subprocess.call(["xmgrace","-nosafe", "-hardcopy", "-batch", batch,size_file,wgd_file,"-graph", "1", "-nxy",fitness_file,"-param", param, "-printfile",printfile])
 
        
if __name__ == "__main__":
    sys.exit(main())

main()

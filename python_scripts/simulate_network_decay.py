import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys
import os.path as path
import os
import numpy as np
import batch.batch_tools as bt
import batch.systematic_compare as sc
import itertools as it
import numpy.ma as ma
from numpy.lib import recfunctions as rf
import random
import note_book_helpers as nbh

def gene_type_deletions(current_gd,ref_dict,del_ref_dict,types=['PUMP','ENZYME','TF']):
    del_dict = del_ref_dict.copy()
    for t in types:
        types_del = len(set(nbh.gene_type_complement(ref_dict,t)) -
                        set(nbh.gene_type_complement(current_gd,t) ) )
        if t in del_dict:
            del_dict[t] = types_del - del_ref_dict[t]            
            del_ref_dict[t] = types_del
        else:
            del_dict[t] = types_del
            del_ref_dict[t] = types_del
    return del_dict

def del_genes(n,prob_func,ids,K_dict):
    avrg_K = sum(K_dict.values())/float(len(K_dict))
    probs_dict = {_id: prob_func(K_dict[_id]/avrg_K) for _id in ids }
    sum_probs = sum(probs_dict.values())
    dels = []
    for i in range(n):
        ##### little change applied
        #avrg_K = sum(K_dict.values())/float(len(K_dict))
        #probs_dict = {_id: prob_func(K_dict[_id]/avrg_K) for _id in ids }
        sum_probs = sum(probs_dict.values())
        #####
        rand = random.uniform(0,sum_probs)
        cum_prob = 0.
        for key,val in probs_dict.items():
            cum_prob += val
            if rand < cum_prob:
                del probs_dict[key]
                sum_probs-=val
                dels.append(key)
                break
    return dels

#doesn't work: deletion probabilities are too high for everyone to
#produce any perceivable skew in evolutionary connectivity
def del_genes2(n,prob_func,ids,K_dict):
    avrg_K = sum(K_dict.values())/float(len(K_dict)) # the average in the reference genome
    # deletion probabilites based on relative connectivities within reference network 
    probs_dict = {_id: prob_func(K_dict[_id]/avrg_K) for _id in ids } 
    dels = []
    #id_set = set(ids)
    for i in range(n):
        while True:
            rand_gene = random.choice(probs_dict.keys())
            p_del = probs_dict[rand_gene]
            if p_del >= 0.:
                print "no chance to delete"
                break
            if random.uniform(0,1) < p_del:
                dels.append(rand_gene)
                del probs_dict[rand_gene]
                break                
    return dels


def simulate_dels(gd,del_dict,wgd_dict,types=None,del_prob_func=None,
                  outdegree=True,per_type=True):
    sim_dict = gd 
    _del_genes = []
    del_items = del_dict.items() if not types else [(t,del_dict[t]) for t in types]
    '''look up number of dels(v) for gene type(k) '''
    if per_type:
        for t,n in del_items: # go through the separate gene types
                              # (TF,enzyme,pump) t=type, n=number of deletions
            if n < 0:
                print "do", n, "dels"
                print wgd_dict
                print
                print del_dict
                print
                print gd
                continue
            type_ids = nbh.gene_type_complement(gd,t)
            if outdegree:
                if t=='TF' and del_prob_func:
                    '''only TFs have an outdegree'''
                    wgd_Ks = nbh.connectivity_dict(wgd_dict,outdegree,t)
                    _del_genes += del_genes(n,del_prob_func,type_ids,wgd_Ks)
                else:
                    _del_genes += random.sample(type_ids,n)
            else:
                '''looking at indegree'''
                if del_prob_func:
                    wgd_Ks = nbh.connectivity_dict(wgd_dict,outdegree,t)                
                    _del_genes += del_genes(v,del_prob_func,type_ids,wgd_Ks)
                else:
                    _del_genes += random.sample(type_ids,n)
    else:
        del_number = sum([n for t,n in del_items])
        gene_ids = gd.keys()
        _del_genes = random.sample(gd.keys(),del_number)
    
    for g in _del_genes:
        del sim_dict[g]
    return sim_dict

def simulate_genome_dicts(real_genome_dicts,wgds_dicts,
                          wgd_nr = -1,types=None,per_type=True,
                          del_prob_func=None,outdegree=False):
    sorted_ = [ (k,real_genome_dicts[k]) for k in sorted(real_genome_dicts.keys()) ]
    wgds_dicts_sorted = [ (k,wgds_dicts[k]) for k in sorted(wgds_dicts.keys()) ]
    simulated_genomes = []
    simulated_genome_dicts = dict()
    if not type(del_prob_func) == list: # make a list of functions for every genome dict
        del_prob_func = [del_prob_func]*len(sorted_)
    elif not len(del_prob_func) == len(sorted_):
        print "number of probability functions unequal to genome dicts "
        return
    if wgds_dicts:
        wgd_gen,wgd_dict = wgds_dicts_sorted[wgd_nr]
        reference_genes = wgd_dict.keys()
        simulation_state = wgd_dict.copy()
        dels_done = dict()

        for dpf,(generation,gd) in zip(del_prob_func,sorted_):
            del_dict = dict()
            if generation > wgd_gen:
                del_dict = gene_type_deletions(gd,wgd_dict,dels_done)
            #print "wgd generation", wgd_gen, "genome generation:", generation

            simulation_state = simulate_dels(simulation_state.copy(),del_dict,
                                             wgd_dict,types,dpf,
                                             outdegree,per_type)
            simulated_genomes.append(simulation_state)
        simulated_genome_dicts = dict(zip(sorted(real_genome_dicts.keys()),
                                          simulated_genomes))
    return simulated_genome_dicts

vsimulate_genome_dicts = (lambda wgd=-1,ts=None,pt=True,
                          func=None,o=False:
                          np.vectorize(lambda gd_wgdd_ohn:
                                       simulate_genome_dicts(gd_wgdd_ohn[0],
                                                             gd_wgdd_ohn[1],
                                                             wgd,ts,pt,func,o)
                                       if gd_wgdd_ohn[3] else None,
                                       otypes=[np.object]))


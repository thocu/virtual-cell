import sys, os, re
from itertools import *
import itertools as it

'''Generates the systematic environments. We define a low, high and
"same" state and generate all combinations of parameters. This set is
filtered to contain those environments where at least 2 parameters
stay "the same", relative to a standard environment'''
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

## path_ = "/home/thocu/WB/config_files/systematic"
## States = enum('LOW','HIGH', 'SAME')
## mapping = [("target-A-in=",[0.25,4,1]),
##            ("target-X-in=",[0.25,4,1]),
##            ("passive-diffusion=",[0.05,0.4,0.1]),
##            ("conversion-rate=",[2,8,4]),
##            ("enzyme-degradation=",[0.5,4,1])]
## possibilities = [States.HIGH,States.LOW,States.SAME]
## prod =  product(possibilities,repeat=5)
## filtered = ifilter(lambda x: sum(imap(lambda y: y == States.SAME ,x))==2 ,prod)
## for i,tup in enumerate(filtered):
##     env_file = open(os.path.join(path_,"systematic_env"+str(i)+".cfg"),'w')
##     for (j,(name,vals)) in zip(tup,mapping):
##         print >> env_file,name+str(vals[j])
##     env_file.close()

States = enum('LOW','HIGH', 'SAME')
mapping = [("A",[0.25,4,1]),
           ("X",[0.25,4,1]),
           ("pd",[0.05,0.4,0.1]),
           ("cr",[2,8,4]),
           ("ed",[0.5,4,1])]
possibilities = [States.HIGH,States.LOW,States.SAME]
prod =  it.product(possibilities,repeat=5)
print States.SAME == States.SAME
filtered = it.ifilter(lambda x: sum(it.imap(lambda y: 1 if y == States.SAME else 0 ,x))==2 ,prod)
for i,tup in enumerate(filtered):
    print i,":",
    for (j,(name,vals)) in zip(tup,mapping):
        print name,str(vals[j]),
    print

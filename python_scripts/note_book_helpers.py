import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys
import os.path as path
import os
import numpy as np
import batch.batch_tools as bt
import batch.systematic_compare as sc
import itertools as it
import numpy.ma as ma
from numpy.lib import recfunctions as rf


def merge_on_axis(matrix,axes=[0,1]):
    shape = matrix.shape
    new_shape=()
    new_axis_shape = 1
    combining = False
    for a,s in enumerate(shape):
        if a not in axes:
            if combining:
                new_shape += (new_axis_shape,)
                combining = False
            new_shape += (s,)
        else:
            new_axis_shape *= s
            combining = True
    reshaped = matrix.reshape(new_shape)
    return reshaped

def plot_3d(x,y,z,subplot):
    redistribute_map = {0.5:0, 1:1, 4:2}
    #subplot = fig.add_subplot() = plt.figure(name)
    ax = subplot

    def color(x,y):
        color = 'b'
        if max(x,y) / min(x,y)==4:
            color = 'r'
        elif max(x,y) / min(x,y)==16:
            color = 'g'
        return color

    red=[]
    blue=[]
    green=[]
    for x, y, env in zip(x,y,z):
        A = env[0]
        X = env[1]
        deg = redistribute_map[env[4]]
        if max(A,X) / min(A,X)==4:
            red.append((x,y,deg))
        elif max(A,X) / min(A,X)==16:
            green.append((x,y,deg))
        else:
            blue.append((x,y,deg))
    ax.scatter( [val[0] for val in red],
               [val[1] for val in red],
               [val[2] for val in red], s=40, c='r',marker='^',label="A X 4 fold diff.")
    ax.scatter( [val[0] for val in blue],
               [val[1] for val in blue],
               [val[2] for val in blue], s=40, c='b',marker='D', label="A X equal" )
    ax.scatter( [val[0] for val in green],
               [val[1] for val in green],
               [val[2] for val in green], s=40, c='g', label="A X 16 fold diff." )
    ax.set_xlabel('fitness')
    ax.set_ylabel('wgds')
    ax.set_zlabel('degradation rate')
    ax.set_zlim3d(0, 2)                    
    ax.set_ylim3d(0, 3)                    
    ax.set_xlim3d(0, 1)
    ax.plot_wireframe(np.array([[0.5,0.75,1],[0.5,0.75,1],[0.5,0.75,1],[0.5,0.75,1]]),np.array([[0,0,0],[1,1,1],[2,2,2],[3,3,3]]),
                np.array([[1,1,1],[1,1,1],[1,1,1],[1,1,1]]),rstride=1, cstride=1,color='lightgrey')
    ax.plot_wireframe(np.array([[0.5,0.75,1],[0.5,0.75,1],[0.5,0.75,1],[0.5,0.75,1]]),np.array([[0,0,0],[1,1,1],[2,2,2],[3,3,3]]),
                np.array([[2,2,2],[2,2,2],[2,2,2],[2,2,2]]),rstride=1, cstride=1,color='lightgrey')

    majorLocator   = plt.MultipleLocator(1)    
    ax.zaxis.set_major_locator(majorLocator)
    ax.yaxis.set_major_locator(majorLocator)
    majorLocator   = plt.MultipleLocator(0.25)
    ax.xaxis.set_major_locator(majorLocator)

    return ax

class SaveFigure:
    def __init__(self,save_base=None):
        if save_base == None:
            self.figure_save_base = "/home/thocu/WB/python_scripts/local_files/"
        elif path.exists(save_base):
            self.figure_save_base = path.abspath(save_base)
        else:
            self.figure_save_base = path.abspath(save_base)
            os.mkdir(self.figure_save_base)
            
    def savefig(self,fig=None,name=None,*args):
        if fig == None and name == None:
            print "Don't know how to save, because no figure or name is provided"
        else:
            if name is None:
                name = fig.get_label()+".svg"
            file_name = path.join(self.figure_save_base,name.replace(" ","_"))
            print "saving picture at:", file_name
            if fig == None:
                plt.savefig(file_name,*args)
            else:
                fig.savefig(file_name,*args)

def seperate_wgd_fitness_plots(Matrix,select_index,output_index,splits,
                               name="subplots",plot_names=None,dims=(2,2)):
    majorLocator   = plt.MultipleLocator(1)
    majorFormatter = plt.FormatStrFormatter('%d')
    arrays = split_numpy(Matrix,select_index,output_index,splits)
    rows = dims[0]
    cols = dims[1]

    fig, axs = plt.subplots(rows,cols, sharex=True, sharey=True,num=name,squeeze=False)
    plt.suptitle(name,fontsize=20)
    if plot_names is None: 
        arrays = izip(arrays,("" for i in it.count()))
    else:
        arrays = zip(arrays,plot_names)
    for i,(array,name) in enumerate(arrays):
        col = i % cols
        row = (i - col) / cols
        ax = axs[row,col]
        if array != None:
            ax.scatter(array[output_index[0]],array[output_index[1]])
        ax.set_title(name +" (" + str(len(array)) + " points)")
        ax.yaxis.set_major_locator(majorLocator)
        ax.yaxis.set_major_formatter(majorFormatter)
        ax.set_ylim((-0.5,4))
        ax.set_xlim((0,1.1))
    return fig
        
def split_numpy(a,select_index,output_index,splits):
    '''split numpy array into a set of arrays depending on a separator tuple and an index'''
    arrays = [ None for i in range(len(splits)+1) ]
    for item,output in zip(a[select_index],a[output_index]):
        placed = False
        if item is ma.masked:
            print 'masked'
            continue
        for i,split in enumerate(splits):
            if item <= split:
                if arrays[i] == None:
                    arrays[i] = output
                else:
                    arrays[i] = ma.vstack((arrays[i],output))
                placed = True
                break
        if not placed:
            if arrays[-1] == None:
                arrays[-1] = output
            else:
                arrays[-1] = ma.vstack((arrays[-1],output))
    return arrays #map(lambda x: x.compressed(), arrays)

#Quick, turn off printing!
class dummyStream:
    ''' dummyStream behaves like a stream but does nothing. '''
    def __init__(self):
        # Copy old print deals
        self.old_printerators=[sys.stdout,sys.stderr,sys.stdin,sys.__stdout__,
                          sys.__stderr__,sys.__stdin__][:]
    def write(self,data): pass
    def read(self,data): pass
    def flush(self): pass
    def close(self): pass

    def SILENT(self):
        # redirect all print deals
        (sys.stdout,sys.stderr,sys.stdin,
         sys.__stdout__,sys.__stderr__,sys.__stdin__) = (dummyStream(),
                                                         sys.stderr,#dummyStream(),
                                                         dummyStream(),
                                                         dummyStream(),
                                                         dummyStream(),
                                                         dummyStream()
                                                         )

    def VERBOSE(self):
        #Turn printing back on!
        (sys.stdout,sys.stderr,sys.stdin,
         sys.__stdout__,sys.__stderr__,sys.__stdin__) = self.old_printerators

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

States = enum('LOW','HIGH', 'SAME')
mapping = [("A",[0.25,4,1]),
           ("X",[0.25,4,1]),
           ("pd",[0.05,0.4,0.1]),
           ("cr",[2,8,4]),
           ("ed",[0.5,4,1])]
possibilities = [States.HIGH,States.LOW,States.SAME]
prod =  it.product(possibilities,repeat=5)
filtered = it.ifilter(lambda x: sum(map(lambda y: 1 if y == States.SAME else 0 ,x))==2 ,prod)
mapped = []
for f in filtered:
    mapped.append(tuple( mapping[i][1][j] for (i,j) in enumerate(f)))
    
envs = dict(enumerate(mapped))

'''vectorization functions'''
env_dtype =  [('A',np.float),('X',np.float),('pd',np.float),('cr',np.float),('ed',np.float)]
def venvs(m,nr_envs=5):
    shape=(m.shape[:])
    envV = np.zeros(shape,dtype = env_dtype)
    for index,value in np.ndenumerate(m[:]):
        envV[index] = envs[value]
    return envV

vwgd_times = np.vectorize(lambda x: bt.Run.WgdsList(x,"global_after",after=x.env_switch[1],
                                        before=x.env_switch[1]+SWITCH_TIME-1),
                     otypes=[np.float])

vindex = np.vectorize(lambda x: x.index[0],otypes=[np.int])

vname_and_place = np.vectorize(lambda x: x.name+" on "+x.host,otypes=[np.str])

vsystematic_index = np.vectorize(lambda x: sc.systematic_index(x.name),otypes=[np.int8])

vaverage_fit = np.vectorize(lambda x: bt.Run.AverageFitness(x,_after=28000,_before=29000),
                            otypes=[np.float])
vmax_fit = lambda time : np.vectorize(lambda x: bt.Run.MaxFitness(x,_after=x.env_switch[1],
                                                 _before=x.env_switch[1] + time - 1)[0],
                        otypes=[np.float])
vmax_size = lambda time: np.vectorize(lambda x: bt.Run.MaxSize(x,size_file="global_after/ancestor_sizes.dat",
                                               _after=x.env_switch[1],
                                               _before=x.env_switch[1] + time - 1)[0],
                        otypes=[np.float])
vwgds_after = lambda time: np.vectorize(lambda x: bt.Run.Wgds(x,"global_after",after=x.env_switch[1],
                                        before=x.env_switch[1] + time - 1),
                     otypes=[np.float])
vwgds_before = lambda _dir="global_before" : np.vectorize(lambda x: bt.Run.Wgds(x,_dir,
                                        before=x.env_switch[1]),
                     otypes=[np.float])
vwgd_times_after = lambda time: np.vectorize(lambda x:bt.Run.WgdList(x,"global_after",after=x.env_switch[1],
                                                        before=x.env_switch[1] + time - 1),
                                otypes=[np.object]) # only record WGD times up till half the simulation duration
vwgd_times_before = np.vectorize(lambda x: bt.Run.WgdList(x,"global_before",
                                                          before=x.env_switch[1]),
                                 otypes=[np.object])
vswitch_times = np.vectorize(lambda run: run.env_switch[1],
                             otypes=[np.float])

vadapt_times_early = np.vectorize(lambda run : run.fit_before_switch[1],
                                  otypes=[np.float],
                                  doc="Return the time at which the run became fit before environment switching")

vadapt_times_late = np.vectorize(lambda run : run.fit_after_switch[1] - run.env_switch[1],
                                 otypes=[np.float], 
                                 doc="Return the time at which the run became fit after environment switch")

vadapt_before = lambda time: np.vectorize(lambda run : run.fit_before_switch[0] and 
                                          run.fit_before_switch[1] <= time, otypes=[np.bool])

vadapt_cutoffs = lambda cutoff: np.vectorize(lambda run : run.fit_after_switch[0] and 
                              (run.fit_after_switch[1] - run.env_switch[1] ) <= cutoff,
                              otypes=[np.bool])
vgene_loss_rate_late = lambda loss_frac,time: np.vectorize(lambda run: bt.Run.LossRate(run,"global_after/ancestor_sizes.dat",
                                                                                  run.env_switch[1],
                                                                                  run.env_switch[1] + time - 1,
                                                                                  loss_frac),
                                                      otypes = [np.float],
                                                      doc="""Return a loss rates in the interval between generations where 
the maximum genome size and subsequent minimum size occured, in genes per generation,
after the environmental switch""")
vgene_loss_rate_early = lambda loss_frac: np.vectorize(lambda run: bt.Run.LossRate(run,"global_before/ancestor_sizes.dat",
                                                                                   0,
                                                                                   run.env_switch[1],loss_frac),
                                                       otypes = [np.float],
                                                       doc="""Return a loss rates in the interval between generations where 
the maximum genome size and subsequent minimum size occured, in genes per generation,
before the environmental switch""")                                                       
vfit_before_switch = np.vectorize(lambda run: run.fit_before_switch[0], otypes=[np.bool],
                                  doc="Return True entries for runs that became fit before environment switching")
vfit_after_switch = lambda time: np.vectorize(lambda run: run.fit_after_switch[0] and
                                                run.fit_after_switch[1] <= (run.env_switch[1] + time - 1),
                                                otypes=[np.bool],
                                                doc="Return True entries for runs that became fit after environment switch")
vsize_at_virtual_switch = lambda delay: np.vectorize(lambda run: bt.Run.MaxSize(run,size_file="global_before/ancestor_sizes.dat",
                                                          _after=run.env_switch[1] - delay -1,
                                                          _before = run.env_switch[1] - delay),
                               otypes = [np.float],doc="Return matrix of genome sizes at the time of environment switching")

vwgds = lambda time: np.vectorize(lambda x: bt.Run.Wgds(x,"global_after",after=x.env_switch[1],
                                        before=x.env_switch[1]+time-1),
                     otypes=[np.float])
vadapt_times = np.vectorize(lambda run : run.fit_after_switch[1] - run.env_switch[1],
                            otypes=[np.float] )

vgenome_dicts = lambda i,s,sd=None,w=True : np.vectorize(lambda run : 
                                                 bt.Run.GenomeDictionaries(run,save_dir=sd,interval=i,step_size=s,
                                                                           switch=w),
                                                 otypes = [np.object])

vwgd_dicts = lambda sd="global_after/",w=True : np.vectorize(lambda runs_wgds : 
                                                 bt.Run.GenomeDictionaries(runs_wgds[0],sd,0,0,
                                                                           switch=w,wgd_times=runs_wgds[2]) if runs_wgds[1] else
                                                             None,
                                                 otypes = [np.object])

vsize_at_switch = np.vectorize(lambda run: bt.Run.MaxSize(run,size_file="global_after/ancestor_sizes.dat",
                                                          _after=run.env_switch[1],
                                                          _before = run.env_switch[1]+1)[0],
                               otypes = [np.float],doc="Return matrix of genome sizes at the time of environment switching")

vfitness_at_switch = np.vectorize(lambda run: bt.Run.MaxFitness(run,fitness_file="global_after/ancestor_standard_fitnesses.dat",
                                                          _after=run.env_switch[1],
                                                          _before = run.env_switch[1]+1)[0],
                               otypes = [np.float],doc="Return matrix of fitnesses at the time of environment switching")
vfitness_at_0 = lambda _file="global/ancestor_standard_fitnesses.dat": np.vectorize(lambda run: bt.Run.MaxFitness(run,fitness_file=_file,
                                                          _after=0,
                                                          _before = 1)[0],
                               otypes = [np.float],doc="Return matrix of fitnesses at the time of environment switching")

vlineage_sizes_after_switch = lambda stps=15,stpsize=1000: np.vectorize(lambda run:
                                                                        bt.Run.LineageSizes(run,"global_after/ancestor_sizes.dat",
                                                                                            run.env_switch[1]+1,stps,stpsize),
                                                                        otypes = [np.object],
                                                                        doc="Return matrix of genome sequences of sizes after environment switching")
    
vmut_rates = lambda time,st=10: np.vectorize(lambda run: bt.Run.MutationRates2(run, _after = run.env_switch[1],
                                                                                _before = run.env_switch[1] + time, steps=st),
                                              otypes = [np.object])
vmut_rates_continue = lambda time,st=10: np.vectorize(lambda run: bt.Run.MutationRates2(run,save_dir = "global/", _after = run.env_switch[1],
                                                                                _before = run.env_switch[1] + time, steps=st),
                                              otypes = [np.object])

vdup_rates = lambda rel = True:np.vectorize(lambda rates_list: map(lambda rates: (rates[0],rates[1][0][1]
                                                                                  if rel else rates[1][0][0]),rates_list),
                                            otypes = [np.object])
vdel_rates = lambda rel = True:np.vectorize(lambda rates_list: map(lambda rates: (rates[0],rates[1][1][1]
                                                                                  if rel else rates[1][1][0]),rates_list),
                                            otypes = [np.object])
vpoint_rates = lambda rel = True:np.vectorize(lambda rates_list: map(lambda rates: (rates[0],rates[1][2][1]
                                                                                    if rel else rates[1][2][0]),rates_list),
                                              otypes = [np.object])
vdup_event_rates = lambda rel = True:np.vectorize(lambda rates_list: map(lambda rates: (rates[0],rates[1][3][1]
                                                                                        if rel else rates[1][3][0]),rates_list),
                                                  otypes = [np.object])
vdel_event_rates = lambda rel = True:np.vectorize(lambda rates_list: map(lambda rates: (rates[0],rates[1][4][1]
                                                                                        if rel else rates[1][4][0]),rates_list),
                                                  otypes = [np.object])
vins_event_rates = lambda rel = True:np.vectorize(lambda rates_list: map(lambda rates: (rates[0],rates[1][5][1]
                                                                                        if rel else rates[1][5][0]),rates_list),
                                                  otypes = [np.object])
vwgd_rates = lambda rel = True:np.vectorize(lambda rates_list: map(lambda rates: (rates[0],rates[1][6][1]
                                                                                  if rel else rates[1][6][0]),rates_list),
                                            otypes = [np.object])

vrates_only = np.vectorize(lambda tup_lst: np.array(map(lambda x: x[1], tup_lst)),otypes = [np.object] )

vfinished = np.vectorize(lambda run: run.finished)

vohnologs = np.vectorize(lambda run: bt.Run.Ohnologs(run), otypes = [np.object])

vwgd_fitness_effects = np.vectorize(lambda runs_wgds: bt.Run.WGD_fitness_effects(runs_wgds[0]) if
                                    runs_wgds[1] else None, otypes = [np.object])

vget_files = lambda f = "": np.vectorize(lambda run: bt.Run.GetFile(run,f), otypes = [np.object])

def sizes_after_switch(_run, timepoints,size_file=None):
    if not size_file:
        size_file = "global_after/ancestor_sizes.dat"
    file_path=os.path.join(_run.name,size_file)        
    if not os.path.exists(file_path):
        return None
    size_vect = []
    for t in timepoints:
        size = bt.Run.MaxSize(_run,size_file=size_file,
                       _after=_run.env_switch[1]+t,
                       _before = _run.env_switch[1]+t+1)[0]
        size_vect.append(size)
    return size_vect

vsizes_after_switchM = (lambda tps,sf=None:
                        np.vectorize(lambda run:
                                     sizes_after_switch(run,tps,size_file=sf),
                                     otypes = [np.object]))
###old vector function, now use sizes_after_switch
## REMOVED ##

def multi_index(_l,index):
    return _l if not len(index) else multi_index(_l[index[0]],index[1:]) 
        
def extract_stat(wgd_stats,nr = None,pos=[2]):
    stat_array = None
    if wgd_stats and not nr == -1: # -1 if data not grouped per wgd
        if nr is None:
            for i,wgd_stat in enumerate(wgd_stats):
                stats = np.array([ multi_index(stat,pos) for stat in wgd_stat ])
                if not i:
                    stat_array = stats
                else:
                    stat_array = np.vstack((stat_array,stats))
            if len(wgd_stats) > 1:
                #pass
                stat_array = np.average(stat_array,0)
        elif len(wgd_stats) > nr:
            stat_array = np.array([ multi_index(stat,pos) for stat in wgd_stats[nr] ])
    elif wgd_stats:
        stat_array = np.array([ multi_index(stat,pos) for stat in wgd_stats]) 
    return stat_array

vextract_stat = (lambda n=None,p=2:
                 np.vectorize(lambda wgd_stats:
                              extract_stat(wgd_stats,n,p),
                              otypes = [np.object]))

def connectivity_dict(gd,outdegree=True,_type=None):
    '''associate genes with their current connectivities'''
    c_dict = dict()
    if not outdegree: 
        c_dict = {g: gene_indegree(g,gd) for g in gd
                  if gd[g]['type'] == _type or not _type}
    else:
        c_dict = {g: gene_outdegree(g,gd) for g in gd
                  if gd[g]['type'] == _type or not _type}
    return c_dict

def gene_type_complement(gd,tp):
    return [ gene_id for gene_id in gd.keys() if gd[gene_id]['type'] == tp ]

def intact_pairs(genome_dicts,wgds_dicts,wgds_ohnos,_type=None,param_dict=dict()):
    gd_sorted = sorted(genome_dicts.items())
    wgds_dicts_sorted = [ wgds_dicts[k] for k in sorted(wgds_dicts.keys()) ]
    wgds_stats = []
    for wgd_dict,(generation,ohnos) in zip(wgds_dicts_sorted,wgds_ohnos):
        gene_type_ohnos = [ (gene_pair[1],gene_pair[2]) for gene_pair in ohnos
                            if _type is None or gene_pair[0] == _type ]
        gene_type_ohnos = [ (p1,p2) for t,p1,p2 in ohnos if _type is None or
                            t ==  _type and  all([wgd_dict[p1][k] == v
                                                  for k,v in param_dict.items()]) and
                            all([wgd_dict[p2][k] == v for k,v in param_dict.items()]) ]
        all_ = list(it.chain(*gene_type_ohnos))
        wgd_stats = []
        if not len(gd_sorted):
            print "empty genome dicts"
        for gd in gd_sorted:
            overall = 0.
            pairs = 0
            genes_present = []
            if not _type:
                genes_present = gd[1].keys()
            else:
                genes_present = [ gene_id for gene_id in gd[1].keys()
                                  #if gd[1][gene_id]['type']== _type ]
                                  if gd[1][gene_id]['type'] == _type and
                                      all([gd[1][gene_id][k] == v
                                           for k,v in param_dict.items()]) ]
            overall = float("-inf"),float("-inf")
            if all_:
                present = len(set.intersection(set(all_),set(genes_present))) 
                overall = present, present/float(len(all_))                
            for pair in gene_type_ohnos:
                if pair[0] in genes_present and pair[1] in genes_present:
                    pairs += 1
            pair_fraction = float("-inf"),float("-inf")
            if gene_type_ohnos:
                pair_fraction = pairs*2, pairs / float(len(gene_type_ohnos))
            pairs_in_overall = float("-inf")
            if overall[1] > 0.:
                pairs_in_overall = pair_fraction[1]/overall[1]
            wgd_stats.append((overall,pair_fraction,pairs_in_overall))
        wgds_stats.append(wgd_stats)
    return wgds_stats
            
vintact_pairs = (lambda _type=None,param_dict=dict():
                 np.vectorize(lambda gd_ohn:
                              intact_pairs(gd_ohn[0], gd_ohn[1],gd_ohn[2],
                                           _type,param_dict)
                              if gd_ohn[3] else None,
                              otypes = [np.object]))

def ohnolog_divergence(genome_dicts,wgds_dicts,wgds_ohnos,_type=None,param="op",ref_pos=0,
                       param_conservation = lambda x,y: x==y):
    '''determine for a certain parameter if WGD duplicates have diverged'''
    wgds_stats = []
    gd_sorted = sorted(genome_dicts.items())
    wgds_dicts_sorted = [ wgds_dicts[k] for k in sorted(wgds_dicts.keys()) ]
    if gd_sorted:
        try:
            ref_gd = gd_sorted[ref_pos][1]
        except IndexError:
            print 'has no data at the reference position'
            return []
    else:
        return []
    reference_genes = []
    if not _type:
        reference_genes = ref_gd.keys()
    else:
        reference_genes = [ gene_id for gene_id in ref_gd.keys()
                            if ref_gd[gene_id]['type'] == _type ]
    first_time = True
    for wgd_dict, (generation,ohnos) in zip(wgds_dicts_sorted,wgds_ohnos):
        gene_type_ohnos = [ (gene_pair[1],gene_pair[2]) for gene_pair in ohnos
                            if _type is None or gene_pair[0] == _type ]
        all = list(it.chain(*gene_type_ohnos))
        wgd_reference_genes = []
        if not _type:
            wgd_reference_genes = wgd_dict.keys()
        else:
            wgd_reference_genes = [ gene_id for gene_id in wgd_dict.keys()
                                    if wgd_dict[gene_id]['type'] == _type ]
        wgd_stats = []
        for gd in gd_sorted:
            genes_present = []
            if not _type:
                genes_present = gd[1].keys()
            else:
                genes_present = [ gene_id for gene_id in gd[1].keys()
                                  if gd[1][gene_id]['type']== _type ]
            present_pairs = [ (p1,p2) for (p1,p2) in gene_type_ohnos
                              if p1 in genes_present and p2 in genes_present ]
            ref_intersection = set.intersection(set(reference_genes),set(genes_present))
            # all genes relative to ref position
            conserved = 0
            for gene in ref_intersection:
                conserved += float(param_conservation(gd[1][gene][param],
                                                      ref_gd[gene][param]))
            all_conservation = float('-inf')
            if len(ref_intersection):
                all_conservation = conserved / float(len(ref_intersection))
            # all genes relative to post wgd genome
            wgd_ref_intersection = set.intersection(set(wgd_reference_genes),
                                                    set(genes_present))
            conserved = 0
            for gene in wgd_ref_intersection:
                conserved +=float(param_conservation(gd[1][gene][param],
                                                     wgd_dict[gene][param]))
            all_wgd_conservation = float('-inf')
            if len(wgd_ref_intersection):
                all_wgd_conservation = conserved / float(len(wgd_ref_intersection))
            # ohnologs
            conserved = 0
            same = 0
            for p1,p2 in present_pairs:
                same +=float(param_conservation(gd[1][p1][param],gd[1][p2][param]))
                try:
                    conserved+= 0.5* float(param_conservation(gd[1][p1][param],
                                                              wgd_dict[p1][param]))
                except KeyError:
                    if first_time:
                        print p1, "not found in", wgd_dict
                        first_time = False 
                try:
                    conserved+= 0.5* float(param_conservation(gd[1][p2][param],
                                                              wgd_dict[p2][param]))                    
                except KeyError:
                    if first_time:
                        print p2, "not found in", wgd_dict
                        first_time = False
            pair_identity = float('-inf')
            pair_conservation = float('-inf')
            if len(present_pairs):
                pair_identity = same / float(len(present_pairs))
                pair_conservation = conserved / float(len(present_pairs))
            # singles 
            present_singles = wgd_ref_intersection - set(list(it.chain(*present_pairs)) )             
            conserved = 0
            for gene in present_singles:
                conserved+=float(param_conservation(gd[1][gene][param],
                                                    wgd_dict[gene][param]))                                          
            single_conservation = float('-inf')
            if len(present_singles):
                single_conservation = conserved / float(len(present_singles))
            wgd_stats.append((all_conservation,all_wgd_conservation,
                              pair_conservation,pair_identity,single_conservation))
        wgds_stats.append(wgd_stats)
    return wgds_stats
                            
vohnolog_divergence = (lambda _type=None,param="op",p=0,
                       pc=lambda x,y: x==y:
                       np.vectorize(lambda gd_wgdd_ohn:
                                    ohnolog_divergence(gd_wgdd_ohn[0],
                                                       gd_wgdd_ohn[1],
                                                       gd_wgdd_ohn[2],
                                                       _type,param,p,pc)
                                    if gd_wgdd_ohn[3] else None,
                                    otypes = [np.object]))

def gene_outdegree(gene_id,cell_dict):
    k = 0
    try: 
        bind_seq = cell_dict[gene_id]['bind']
        for gene in cell_dict.keys():
            if cell_dict[gene]['op'] == bind_seq:
                k += 1
    except KeyError:
        #print 'this is not a TF'
        k = -1
    return k

def gene_indegree(gene_id,cell_dict):
    k = 0
    operator = cell_dict[gene_id]['op']
    for gene in cell_dict.keys():
        if cell_dict[gene]['type'] == 'TF':
            if cell_dict[gene]['bind'] == operator:
                k += 1
    return k

### rank ancestral genes on the basis of their survival times output
### together with the connectivity of the gene to analyze relationship
### of survival rate and connectivity of genes
def ohno_deletion_ranks(cell_dicts,wgds_dicts,wgds_ohnos,_type=None,outdegree=False):
    wgd_degree_rankss = []
    sorted_ = [ cell_dicts[k] for k in sorted(cell_dicts.keys()) ]
    wgds_dicts_sorted = [ wgds_dicts[k] for k in sorted(wgds_dicts.keys()) ]
    '''there may be multiple rounds of wgd'''
    if not sorted_ or not wgds_dicts_sorted:
        return wgd_degree_rankss
    for wgd_dict,(generation,wgd_ohnos) in zip(wgds_dicts_sorted,wgds_ohnos):
        '''select the pairs of a specific gene type'''
        gene_type_ohnos = [ (gene_pair[1],gene_pair[2]) for gene_pair in wgd_ohnos
                            if _type is None or gene_pair[0] == _type ]
        '''all genes of a type NOT in pairs'''
        all = list(it.chain(*gene_type_ohnos))
        wgd_degree_ranks = []
        reference_genes = []
        if not _type:
            reference_genes = wgd_dict.keys()
        else:
            reference_genes = [ gene_id for gene_id in wgd_dict.keys()
                                if wgd_dict[gene_id]['type'] == _type ]
        total_rank = len(reference_genes)
        cumulative_K = 0.
        for gene in reference_genes:
            if not outdegree:
                cumulative_K+= gene_indegree(gene,wgd_dict)
            else:
                cumulative_K+= gene_outdegree(gene,wgd_dict)
        average_K = cumulative_K / len(reference_genes)
        prev_rank = 0
        this_rank = 0
        prev_intersection = set(reference_genes)
        for i,cd in enumerate(sorted_):
            if not i:
                continue
            genes_present = []
            if not _type:
                genes_present = cd.keys()
            else:
                genes_present = [ gene_id for gene_id in cd.keys()
                                  if cd[gene_id]['type'] == _type ]
            present_pairs = [ (one,two) for (one,two) in gene_type_ohnos
                              if one in genes_present and two in genes_present ]
            intersection = set.intersection(set(reference_genes),set(genes_present))
            change = prev_intersection - intersection
            prev_rank = this_rank
            this_rank += len(change)
            '''all retained from WGD, not only ohnologs'''
            used_rank = this_rank
            if len(change)>1:
                used_rank = prev_rank + (this_rank - prev_rank)/2.
            for gene in change:
                if not outdegree:
                    virtual = gene_indegree(gene,wgd_dict)/average_K
                    degree_rank = (virtual,used_rank/float(total_rank))
                    wgd_degree_ranks.append(degree_rank)
                else:
                    virtual = gene_outdegree(gene,wgd_dict)/average_K
                    degree_rank = (virtual,used_rank/float(total_rank))
                    wgd_degree_ranks.append(degree_rank)
            prev_intersection -= change # so we can compare the
                                        # next gene presence with
                                        # current gene presence
        prev_rank = this_rank
        this_rank += len(prev_intersection)
        used_rank = this_rank
        for gene in prev_intersection:  # the remaining genes
            if not outdegree:
                virtual = gene_indegree(gene,wgd_dict)/average_K
                degree_rank = (virtual,used_rank/float(total_rank))
                wgd_degree_ranks.append(degree_rank)
            else:
                virtual = gene_outdegree(gene,wgd_dict)/average_K
                degree_rank = (virtual,used_rank/float(total_rank))
                wgd_degree_ranks.append(degree_rank)
        wgd_degree_rankss.append(wgd_degree_ranks)
    return wgd_degree_rankss

vohnolog_deletion_ranks = (lambda out=False,t=None:
                           np.vectorize(lambda gd_wgdd_ohn:
                                        ohno_deletion_ranks(gd_wgdd_ohn[0],
                                                            gd_wgdd_ohn[1],
                                                            gd_wgdd_ohn[2],t,out)
                                        if gd_wgdd_ohn[3] else None,
                                        otypes=[np.object]))

#determine deletion probabilities for genes, based on survival
#percentages within connectivity bins
def ohno_binned_survive_probs(cell_dicts,wgds_dicts,wgds_ohnos,compare_pos=-1,
                          bins=[0.,0.25,0.75,1.25,2.],_type=None,outdegree=False):
    wgd_survive_probsss = [] # CL
    sorted_ = [ (k,cell_dicts[k]) for k in sorted(cell_dicts.keys()) ]
    wgds_dicts_sorted = [ (k,wgds_dicts[k]) for k in sorted(wgds_dicts.keys()) ]
    '''there may be multiple rounds of wgd'''
    if not sorted_ or not wgds_dicts_sorted:
        return wgd_survive_probss
    for (wgd_id,wgd_dict),(wgd_gen,wgd_ohnos) in zip(wgds_dicts_sorted,wgds_ohnos):
        '''select the pairs of a specific gene type'''
        gene_type_ohnos = [ (gene_pair[1],gene_pair[2]) for gene_pair in wgd_ohnos
                            if _type is None or gene_pair[0] == _type ]
        '''all genes of a type NOT in pairs'''
        all = list(it.chain(*gene_type_ohnos))
        wgd_genes = []
        if not _type:
            wgd_genes = wgd_dict.keys()
        else:
            wgd_genes = [ gene_id for gene_id in wgd_dict.keys()
                                if wgd_dict[gene_id]['type'] == _type ]
        reference_genes = wgd_genes
        degrees = []
        if not outdegree:
            degrees = [ gene_indegree(gene,wgd_dict) for gene in reference_genes ]
        else:
            degrees = [ gene_outdegree(gene,wgd_dict) for gene in reference_genes ]
        WGD_average_degree = sum(degrees)/float(len(degrees))
         # relative degrees of genes in the reference gene set:
        rel_degrees = map(lambda x: x/WGD_average_degree,degrees)
        # make a histogram of the relative degrees of WGD genes
        hist_wgd, bin_edges = np.histogram(np.array(rel_degrees),np.array(bins))
        if not compare_pos:
            cds = sorted_
        else:
            cds = [sorted_[compare_pos]]
        wgd_survive_probss = [] #NL

        hist_ref = hist_wgd #NL
        # NL output survival probabilities for all intervals separately
        for cell_id,cd in cds:
            if cell_id < wgd_id:
                wgd_survive_probs = [float('nan') for x in list(hist_wgd) ]
                shift_degrees = [float('nan') for x in list(hist_wgd) ]
                wgd_survive_probss.append((wgd_survive_probs,shift_degrees))
                continue
            #OL cd = sorted_[compare_pos] # default case we compare with the last genome save
            genes_present = []
            if not _type:
                genes_present = cd.keys()
            else:
                genes_present = [ gene_id for gene_id in cd.keys()
                                  if cd[gene_id]['type'] == _type ]
            present_pairs = [ (one,two) for (one,two) in gene_type_ohnos
                              if one in genes_present and two in genes_present ]
            intersection = set.intersection(set(reference_genes),set(genes_present))

            v_degrees = []
            r_degrees = []
            if not outdegree:
                v_degrees = [ gene_indegree(gene,wgd_dict) for gene in intersection ]
                r_degrees = [ gene_indegree(gene,cd) for gene in intersection ]
            else :
                v_degrees = [ gene_outdegree(gene,wgd_dict) for gene in intersection ]
                r_degrees = [ gene_outdegree(gene,cd) for gene in intersection ]
            rel_degrees = map(lambda x: x/WGD_average_degree,v_degrees)
            hist_v, bin_edges = np.histogram(np.array(v_degrees),
                                             bins=np.array(bins)*WGD_average_degree)
            hist_r, bin_edges = np.histogram(np.array(r_degrees),bin_edges)
            shift_degrees = [ r/float(v) if v else float('nan')
                              for (r,v) in zip(hist_r,hist_v) ]
            hist_cd, bin_edges = np.histogram(np.array(rel_degrees),np.array(bins))
            wgd_survive_probs = [ c/float(ref) if ref else float('nan')
                             for (c,ref) in zip(list(hist_cd),list(hist_ref)) ] #CL from hist_wgd to hist_ref
            wgd_survive_probss.append((wgd_survive_probs,shift_degrees))

            reference_genes=intersection # NL
            #degrees = []
            #if not outdegree:
            #    degrees = [ gene_indegree(gene,wgd_dict) for gene in reference_genes ]
            #else:
            #    degrees = [ gene_outdegree(gene,wgd_dict) for gene in reference_genes ]

             # relative degrees of genes in the reference gene set:
            #rel_degrees = map(lambda x: x/WGD_average_degree,degrees)
            # make a histogram of the relative degrees of WGD genes
            #hist_ref, bin_edges = np.histogram(np.array(rel_degrees),np.array(bins))
            hist_ref = hist_cd
            
        wgd_survive_probsss.append(wgd_survive_probss) # CL
    return wgd_survive_probsss
        
vohnolog_binned_survive_probs_ = (lambda out=False,t=None,
                                  b=[0.,0.25,0.75,1.25,1.75,float('Inf')],p=-1:
                                  np.vectorize(lambda gd_wgdd_ohn:
                                               ohno_binned_survive_probs(gd_wgdd_ohn[0],
                                                                         gd_wgdd_ohn[1],
                                                                         gd_wgdd_ohn[2],
                                                                         _type=t,outdegree=out,
                                                                         compare_pos=p,              
                                                                         bins=b)
                                               if gd_wgdd_ohn[3] else None, otypes=[np.object]))



def gene_degree(gene,gene_dict,out=True):
    return gene_outdegree(gene,gene_dict) if out else gene_indegree(gene,gene_dict)


#Try reimplementing this, refactoring etc
def evolution_of_connectivities(cell_dicts,wgds_dicts=None,wgds_ohnos=[],_type=None,ref_indices=None,outdegree=False):
    ref_Kss = []
    if not wgds_ohnos:
        wgds_ohnos = []
    
    sorted_ = [ (k,cell_dicts[k]) for k in sorted(cell_dicts.keys()) ]
    if wgds_dicts: # in this case, wgd_dicts become references
        ref_dicts = [ (k,wgds_dicts[k]) for k in sorted(wgds_dicts.keys()) ]
    else:          # use some or all of cell_dicts as reference dicts
        ref_dicts = [sorted_[i] for i in ref_indices] if (ref_indices and cell_dicts) else sorted_

    '''there may be multiple rounds of wgd'''
    # if no ohno dict is supplied we zip with 'None' entries as ohno dicts
    for (ref_id,ref_dict),wgd_ohnos in it.izip_longest(ref_dicts,wgds_ohnos):
        if wgd_ohnos:
            (wgd_gen,wgd_ohnos) = wgd_ohnos
        '''select the pairs of a specific gene type'''
        # will be empty for empty wgds_ohnos
        if wgd_ohnos:
            gene_type_ohnos = [ (gene_pair[1],gene_pair[2]) for gene_pair in wgd_ohnos
                                if _type is None or gene_pair[0] == _type ]
        else:
            gene_type_ohnos = []
        ref_Ks = []
        reference_genes = []
        if not _type:
            reference_genes = ref_dict.keys()
        else:
            reference_genes = [ gene_id for gene_id in ref_dict.keys()
                                if ref_dict[gene_id]['type'] == _type ]
        '''all retained from WGD, not only ohnologs'''
        allREFKs = [ gene_degree(gene,ref_dict,outdegree) for gene in reference_genes] 

        allREFKav = float(np.mean(allREFKs)) # nan if empty

        for cell_id,cd in sorted_:
            if cell_id <= ref_id: # only compare after the reference gene set
                na = float('-inf')
                na_pair = (na,na)
                ref_Ks.append((na_pair,na_pair,na_pair,
                           na_pair,na_pair,na_pair,na_pair,
                           na_pair,na_pair))
                continue
            genes_present = []
            if not _type:
                genes_present = cd.keys()
            else:
                genes_present = [ gene_id for gene_id in cd.keys()
                                  if cd[gene_id]['type'] == _type ]
            all_presentKs = [ gene_degree(gene,cd,outdegree) for gene in genes_present ]
            '''connectivities for all present'''
            all_presentKav = float(np.mean(all_presentKs))

            '''Now calculate the intersection'''
            present_pairs = [ (one,two) for (one,two) in gene_type_ohnos
                              if one in genes_present and two in genes_present ]
            intersection = set.intersection(set(reference_genes),set(genes_present))
            '''all retained from WGD, not only ohnologs'''
            allKs_real = [ gene_degree(gene,cd,outdegree) for gene in intersection ]
            allKs_virtual = [gene_degree(gene,ref_dict,outdegree) for gene in intersection ]
            allKav = float(np.mean(allKs_real)),float(np.mean(allKs_virtual))
            '''looking at retained pairs exclusively'''
            pairKs_real = [ gene_degree(gene,cd,outdegree) for gene in list(it.chain(*present_pairs)) ]
            pairKs_virtual = [ gene_degree(gene,ref_dict,outdegree) for gene in list(it.chain(*present_pairs)) ]
            pairKav = float(np.mean(pairKs_real)),float(np.mean(pairKs_virtual))

            real_pair_relativeKav = float("-inf")
            virtual_pair_relativeKav = float("-inf")
            if pairKs_real:
                real_pair_relativeKav = 1.
                virtual_pair_relativeKav = 1.
            if allKav[0] > 0.:
                real_pair_relativeKav = pairKav[0] / allKav[0]
            if allKav[1] > 0.:
                virtual_pair_relativeKav = pairKav[1] / allKav[1]
            relative_pairKav = (real_pair_relativeKav,virtual_pair_relativeKav)
            '''compare WGD pairs to all genes present at time point'''
            real_pair_relative_allKav = float("-inf")
            virtual_pair_relative_allKav = float("-inf")
            if pairKs_real:
                real_pair_relative_allKav = 1.
                virtual_pair_relative_allKav = 1.
            if all_presentKav > 0.: # always relative to the 'real' 
                                    # present connectivies (and not to
                                    # 'virtual'), because not all
                                    # current present genes were
                                    # present at WGD
                real_pair_relative_allKav = pairKav[0] / all_presentKav
                virtual_pair_relative_allKav = pairKav[1] / all_presentKav
            relative_to_all_pairKav = (real_pair_relative_allKav,virtual_pair_relative_allKav)
            ''''''
            #singles
            present_singles = intersection - set(list(it.chain(*present_pairs)))
            singleKs_real = [gene_degree(gene,cd,outdegree) for gene in present_singles ]
            singleKs_virtual = [gene_degree(gene,ref_dict,outdegree) for gene in present_singles ]
            singleKav = float(np.mean(singleKs_real)),float(np.mean(singleKs_virtual))
            real_single_relativeKav = float("-inf")
            virtual_single_relativeKav = float("-inf")
            if singleKs_real:
                real_single_relativeKav = 1.
                virtual_single_relativeKav = 1. 
            if allKav[0] > 0.:
                real_single_relativeKav = singleKav[0] / allKav[0]
            if allKav[1] > 0.:
                virtual_single_relativeKav = singleKav[1] / allKav[1]
            relative_singleKav = (real_single_relativeKav,virtual_single_relativeKav)
            '''scale connnectivities to average at WGD time'''
            real_single_scaledKav = float("-inf")
            virtual_single_scaledKav = float("-inf")

            real_single_relative_allKav = float("-inf")
            virtual_single_relative_allKav = float("-inf")
            if singleKs_real:
                real_single_relative_allKav = 1.
                virtual_single_relative_allKav = 1.
            if all_presentKav > 0.:
                real_single_relative_allKav = singleKav[0] / all_presentKav
                virtual_single_relative_allKav = singleKav[1] / all_presentKav
            relative_to_all_singleKav = (real_single_relative_allKav,virtual_single_relative_allKav)
            
            if singleKs_real:
                real_single_scaledKav = 1.
                virtual_single_scaledKav = 1. 
            if allREFKav > 0.:
                real_single_scaledKav = singleKav[0] / allREFKav
            if allREFKav > 0.:
                virtual_single_scaledKav = singleKav[1] / allREFKav
            scaled_singleKav = (real_single_scaledKav,virtual_single_scaledKav)
            real_pair_scaledKav = float("-inf")
            virtual_pair_scaledKav = float("-inf")
            if pairKs_real:
                real_pair_scaledKav = 1.
                virtual_pair_scaledKav = 1.
            if allREFKav > 0.:
                real_pair_scaledKav = pairKav[0] / allREFKav
            if allREFKav > 0.:
                virtual_pair_scaledKav = pairKav[1] / allREFKav
            scaled_pairKav = (real_pair_scaledKav,virtual_pair_scaledKav)
            ref_Ks.append((allKav,pairKav,relative_pairKav,
                           singleKav,relative_singleKav,
                           scaled_pairKav,scaled_singleKav,
                           relative_to_all_pairKav,relative_to_all_singleKav))
            
        ref_Kss.append(ref_Ks)
    return ref_Kss


vevolution_of_connectivities = (lambda out=False,t=None,wgd=False,ind=None:
                                np.vectorize(lambda gd_wgdd_ohn:
                                             evolution_of_connectivities(gd_wgdd_ohn[0],
                                                                         gd_wgdd_ohn[1],
                                                                         gd_wgdd_ohn[2],t,ind,out)
                                             if (gd_wgdd_ohn[3] == wgd) else None
                                             , otypes=[np.object]))

def ohnolog_connectivities(cell_dicts,wgds_dicts,wgds_ohnos,_type=None,outdegree=False):
    wgd_Css = []
    sorted_ = [ cell_dicts[k] for k in sorted(cell_dicts.keys()) ]
    wgds_dicts_sorted = [ wgds_dicts[k] for k in sorted(wgds_dicts.keys()) ]
    '''there may be multiple rounds of wgd'''
    for wgd_dict,(generation,wgd_ohnos) in zip(wgds_dicts_sorted,wgds_ohnos):
        '''select the pairs of a specific gene type'''
        gene_type_ohnos = [ (gene_pair[1],gene_pair[2]) for gene_pair in wgd_ohnos if _type is None or gene_pair[0] == _type ]
        '''all genes of a type NOT in pairs'''
        all = list(it.chain(*gene_type_ohnos))
        wgd_Cs = []
        reference_genes = []
        if not _type:
            reference_genes = wgd_dict.keys()
        else:
            reference_genes = [ gene_id for gene_id in wgd_dict.keys() if wgd_dict[gene_id]['type'] == _type ]
        for cd in sorted_:
            genes_present = []
            if not _type:
                genes_present = cd.keys()
            else:
                genes_present = [ gene_id for gene_id in cd.keys() if cd[gene_id]['type'] == _type ]
            present_pairs = [ (one,two) for (one,two) in gene_type_ohnos if one in genes_present and two in genes_present ]
            intersection = set.intersection(set(reference_genes),set(genes_present))
            all_connectivities = []
            '''all retained from WGD, not only ohnologs'''
            for gene in intersection:
                if not outdegree:
                    real = gene_indegree(gene,cd)
                    virtual = gene_indegree(gene,wgd_dict)
                    all_connectivities.append((real,virtual))
                else:
                    real = gene_outdegree(gene,cd)
                    virtual = gene_outdegree(gene,wgd_dict)
                    all_connectivities.append((real,virtual))
            wgd_Cs.append(all_connectivities)
        wgd_Css.append(wgd_Cs)
    return wgd_Css

vohnolog_connectivities = (lambda out=False,t=None:
                           np.vectorize(lambda gd_wgdd_ohn:
                                        ohnolog_connectivities(gd_wgdd_ohn[0],
                                                               gd_wgdd_ohn[1],
                                                               gd_wgdd_ohn[2],t,out)
                                        if gd_wgdd_ohn[3] else None, otypes=[np.object]))

def genes_connectedness(cell_dicts,reference_pos=0,_type=None,outdegree=False):
    Kss = []
    sorted_ = [ cell_dicts[k] for k in sorted(cell_dicts.keys()) ] 
    try:
        reference = sorted_[reference_pos]
    except IndexError:
        print 'has no data at the reference position'
        return []
    reference_genes = []
    if not _type:
        reference_genes = reference.keys()
    else:
        reference_genes = [ gene_id for gene_id in reference.keys()
                            if reference[gene_id]['type'] == _type ]
    for cd in sorted_:
        genes_present = []
        if not _type:
            genes_present = cd.keys()
        else:
            genes_present = [ gene_id for gene_id in cd.keys()
                              if cd[gene_id]['type'] == _type ]
        intersection = set.intersection(set(reference_genes),set(genes_present))
        '''real connectedness'''
        Ks = []
        for gene in intersection:
            if not outdegree:
                real = gene_indegree(gene,cd)
                virtual = gene_indegree(gene,reference)
                Ks.append((real,virtual))
            else:
                real = gene_outdegree(gene,cd)
                virtual = gene_outdegree(gene,reference)
                Ks.append((real,virtual))
        Kav = (float("-inf"),float("-inf"))
        if len(Ks):
            Kav = (sum([ K[0] for K in Ks ])/float(len(Ks)),
                   sum([ K[1] for K in Ks ])/float(len(Ks)))
        Kss.append(Kav)
    return Kss

vgenes_connected = (lambda out=False,t=None,p=0:
                    np.vectorize(lambda cd:
                                 genes_connectedness(cd,p,t,out),
                                 otypes=[np.object]))

def genome_diffsM(cell_dicts,first_pos=0,compare_pos=None,_type=None):
    '''Matrix version of genome diffs: make the diffs for all
    positions in cell_dicts, returning a list of intersects'''
    sorted_ = [ cell_dicts[k] for k in sorted(cell_dicts.keys()) ]
    intersects = []
    if not compare_pos:
        compare_pos = range(len(sorted_))
    try:
        first = sorted_[first_pos]
    except IndexError:
        print 'empty genome dict'
        return None
    if not _type:
        k1 = first.keys()
    else:
        k1 = [ gene_id for gene_id in first.keys() if first[gene_id]['type']== _type ]
    for i in compare_pos:
        compare = sorted_[i]
        k2 = compare.keys()
        intersection = set.intersection(set(k1),set(k2))
        intersect = 0.
        if len(k1) > 0:
            intersect = float(len(intersection))/float(len(k1))
        intersects.append(intersect)
    return intersects
    
vgenome_diffsM = (lambda f=0,p=None,t=None:
                  np.vectorize(lambda cd:
                               genome_diffsM(cd,first_pos=f,
                                             compare_pos=p,_type=t),
                               otypes=[np.object]))

def construct_recarray(matrices, dtypes = None):
    recarray = None
    typed_mats = []
    if not dtypes:
        recarray = rf.merge_arrays(matrices,flatten=True,usemask=True)
    elif len(matrices) == len(dtypes):
        for m,d in zip(matrices,dtypes):
            typed_mats.append(m.astype(d))
        recarray = rf.merge_arrays(typed_mats,flatten=True,usemask=True)
    return recarray
            
def rates_to_matrix(rates):
    rates_list = rates[~rates.mask].tolist() 
    b = None
    for i,a in enumerate(rates_list):
        if b is None:
            b = a
        else:
            try:
                b = np.vstack((np.array(a),b))
            except ValueError:
                print "cannot stack arrays of shape", a.shape, b.shape 
                continue
    return b            

def gene_divergence(genome_dicts,_type=None,param="op",ref_pos=0,compare=None):
    '''determine for a certain parameter if WGD duplicates have diverged'''
    if not compare:
        compare = lambda x,y: x==y
    divergence_stats = []
    gd_sorted = sorted(genome_dicts.items())
    if gd_sorted:
        try:
            ref_gd = gd_sorted[ref_pos][1]
        except IndexError:
            print 'has no data at the reference position'
            return []
    else:
        return []
    reference_genes = []
    if not _type:
        reference_genes = ref_gd.keys()
    else:
        reference_genes = [ gene_id for gene_id in ref_gd.keys()
                            if ref_gd[gene_id]['type'] == _type ]
    first_time = True
    for gd in gd_sorted:
        genes_present = []
        if not _type:
            genes_present = gd[1].keys()
        else:
            genes_present = [ gene_id for gene_id in gd[1].keys()
                              if gd[1][gene_id]['type']== _type ]
        ref_intersection = set.intersection(set(reference_genes),set(genes_present))
        # all genes relative to ref position
        conserved = 0
        for gene in ref_intersection:
            if compare(gd[1][gene][param],ref_gd[gene][param]):#gd[1][gene][param] == ref_gd[gene][param]:
                conserved +=1
        all_conservation = float('-inf')
        if len(ref_intersection):
            all_conservation = conserved / float(len(ref_intersection))
        # all genes relative to post wgd genome
        divergence_stats.append((all_conservation,))
    return divergence_stats
                            
vgene_divergence = (lambda _type=None,param="op",p=0,compare=None:
                    np.vectorize(lambda gd: gene_divergence(gd,_type,param,p,compare),
                                 otypes = [np.object]))

def the_numbers(genome_dicts,wgds_dicts,wgds_ohnos,_type=None,param_dict=dict()):
    gd_sorted = sorted(genome_dicts.items())
    wgds_dicts_sorted = [ wgds_dicts[k] for k in sorted(wgds_dicts.keys()) ]
    wgds_stats = []
    for wgd_dict,(generation,ohnos) in zip(wgds_dicts_sorted,wgds_ohnos):
        gene_type_ohnos = [ (gene_pair[1],gene_pair[2]) for gene_pair in ohnos
                            if _type is None or gene_pair[0] == _type ]
        gene_type_ohnos = [ (p1,p2) for t,p1,p2 in ohnos if _type is None or
                            t ==  _type and  all([wgd_dict[p1][k] == v
                                                  for k,v in param_dict.items()]) and
                            all([wgd_dict[p2][k] == v for k,v in param_dict.items()]) ]
        all_ = list(it.chain(*gene_type_ohnos))
        wgd_stats = []
        if not len(gd_sorted):
            print "empty genome dicts"
        for gd in gd_sorted:
            overall = 0.
            pairs = 0
            genes_present = []
            if not _type:
                genes_present = gd[1].keys()
            else:
                genes_present = [ gene_id for gene_id in gd[1].keys()
                                  if gd[1][gene_id]['type'] == _type and
                                  all([gd[1][gene_id][k] == v
                                       for k,v in param_dict.items()]) ]
            in_genome = len(genes_present)
            present = float("-inf")
            present_frac = float("-inf")
            if  all_:
                present = len(set.intersection(set(all_),set(genes_present))) 
                if genes_present:
                    present_frac = present/float(in_genome) 
            conserved = present, present_frac
            conserved_in_pairs = float("-inf")
            conserved_in_pairs_frac = float("-inf")
            genes_in_pairs = 0
            for pair in gene_type_ohnos:
                if pair[0] in genes_present and pair[1] in genes_present:
                    genes_in_pairs += 2
            if all_:
                conserved_in_pairs = genes_in_pairs
                if genes_present:
                    conserved_in_pairs_frac =  genes_in_pairs/float(in_genome)
            pair_conservation = conserved_in_pairs,conserved_in_pairs_frac
            wgd_stats.append((in_genome,conserved,pair_conservation))
        wgds_stats.append(wgd_stats)
    return wgds_stats

vthe_numbers = (lambda _type=None,param_dict=dict():
                np.vectorize(lambda gd_wgdd_ohn:
                             the_numbers(gd_wgdd_ohn[0],gd_wgdd_ohn[1],gd_wgdd_ohn[2],
                                         _type,param_dict)  if gd_wgdd_ohn[3] else None, 
                             otypes = [np.object]))

def per_type_stats(genome_dicts,_types = ['PUMP','ENZYME','TF']):
    gd_sorted = sorted(genome_dicts.items())
    per_type_stats = []
    if not len(gd_sorted):
        print "empty genome dicts"
    for gd in gd_sorted:
        overall = 0.
        pairs = 0
        genes_present = [ [] for i in _types ]
        for i,_type in enumerate(_types):
            #print gd[1]
            #print
            genes_present[i] = sum([ 1 for gene_id in gd[1].keys()
                                 if gd[1][gene_id]['type'] == _type ])
        in_genome = sum(genes_present)
        per_type_stats.append((genes_present,map(lambda x: x/float(in_genome) if x else 0, genes_present)))
    return per_type_stats

vper_type_stats = np.vectorize(lambda gd_wgdd_ohn:
                             per_type_stats(gd_wgdd_ohn[0]), 
                             otypes = [np.object])


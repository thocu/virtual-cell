/*******************************************************************
all assignment operators default

*************-TODO-************************************************


********************************************************************/



//#include "my_random.hpp"
#include "gracesc.h"
#include <map>

#ifndef PETRIDISH_HPP_
#define PETRIDISH_HPP_

#include "cell.hpp"

using namespace std;

class PetriDish{
	
 public:
  PetriDish(string,int);
  PetriDish(string,int,int);
  PetriDish(int,int,string,string,int,int,int,int,int,double,double);
  PetriDish(int,int,int,string,int);

  string getSaveDirectory(){return savedirectory;};
  double get_mutation_rate(){ return mutationrate;};
  double get_fitness_max(){ return fitness_max;};
  int get_generations() const { return generations;};
  void set_drawings(int i){ drawings = i;};
  void set_pop_size(int i){ pop_size = i ; };
  void set_change_rate(double i){ change_rate = i ; };
  void set_mutation_rate(double i){ mutationrate = i ; };
  unsigned int get_pop_size(){ return pop_size;};

  void drawGraphs(list<Cell *> &);

  double FitnessFunction(double);
  void storeFitness(list<Cell *>&,int);
  void storeScaledFitness(list<Cell *>&);
  void storeProduction(list<Cell *>&,int);
  void storeReproduction(list<Cell *>&);
  void store_expression_A_X(list<Cell *>&,int);

  void saveFitness_Max_Average();
  void saveParams();
  void printCells(list<Cell *>&);
  string toString();
  void GracePlot();
  void setGrace();
  void exitGrace();

  Cell* mutateCell1(Cell *,double);
  Cell* mutateCell2(Cell *,double);
  Cell* mutateCell3(Cell *,double);
  Cell* mutateCell4(Cell *,double);
  Cell* mutateCell5(Cell *,double);
  Cell* mutateCell6(Cell *,double);
  Cell* mutateCell7(Cell *,double);

  void mutateCells();
  void resetConc(list<Cell *> &); 
  void set_Aout(double);
  void set_Aout_3env(double);
 
  void updateAncestors(list<Cell*>::iterator);
  void addCell(Cell *);
  void cleanList();
  void reproduce();
  void reproduce2();
  void clone_ancestor(int);

  void updateStep(list<Cell *> &);  
  void step();
  void run(int);
  void step_sparse();
  void run_sparse(int);
  void AnalyseFitness(list<Cell *> &,int);

  void SaveDish(const string) ;
  void RestoreDish(const string, const string,int,int);
  void CreateOutFile(const string dirname);
  void AnalysePop();
  void AnalyseStep(list<Cell *> &);
  void PrintStep(list<Cell *> &);

  const void findRoots();
  void buildAncestors(int);
  void buildTreeList(list<Cell *> &,int,int);
  void addNodes(list<Cell *> &, Cell *, int, int, int);
  void drawAncestorTree(int,int,int);

  list<Cell *> getRoots(){return roots;};
  int distanceLUCA();
  int distanceFittest(Cell *);
  void ReconstructNetworks(list<Cell *> &);
  void OrderGenes(list<Cell *> &);

  list<Cell*> get_cells(){ return cells; };
  list<Cell*>::iterator get_cells_iter(){ return cells_iter;};
  //  gracesc::XmgraceGraph graph(int i){return grace[i];};
  gracesc::Xmgrace grace;

 private:
  PetriDish(const PetriDish&);
  multimap<double,list<Cell*>::iterator> ranking;		
  multimap<double,list<Cell*>::iterator>::iterator rank_iter;		
  list<Cell*> cells;
  list<Cell*>::iterator cells_iter;

  list<Cell *>::iterator ancestor_iter;
  list<Cell *> ancestors;
  list<Cell *>::iterator roots_iter;
  list<Cell *> roots;
  pair<Cell *,int> LUCA;
  pair<Cell *,int> generation_best;
  unsigned int pop_size;
  int Time; //obsoleted, now using real time (see below)
  double _time;
  int generations;
  double mutationrate;        
  double change_rate;         
  int updatesteps;
  double fitness_max;
  double fitness_average;
  int complexity_max;
  double complexity_average;
  double product_gene_ratio_avrg;   
  double product_gene_ratio_max;    
  int start_nmbr_genes;
  int pop_seed_used;
  int evo_seed_used;
  int env_seed_used;          
  string savedirectory;
  string grace_par_file;   
  ofstream fout;
  ofstream fout2;
  bool Achanged;
  int drawings;
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){
    cout << "serializing petridish " << endl;

    ar & BOOST_SERIALIZATION_NVP(cells);
    ar & BOOST_SERIALIZATION_NVP(ancestors);
    ar & BOOST_SERIALIZATION_NVP(roots)   ;  
    ar & BOOST_SERIALIZATION_NVP(pop_size); 
    ar & BOOST_SERIALIZATION_NVP(Time);
    ar & BOOST_SERIALIZATION_NVP(generations);
    ar & BOOST_SERIALIZATION_NVP(mutationrate);
    ar & BOOST_SERIALIZATION_NVP(change_rate);
    ar & BOOST_SERIALIZATION_NVP(updatesteps);    
    ar & BOOST_SERIALIZATION_NVP(fitness_max);
    ar & BOOST_SERIALIZATION_NVP(fitness_average);
    ar & BOOST_SERIALIZATION_NVP(complexity_max);
    ar & BOOST_SERIALIZATION_NVP(complexity_average);    
    ar & BOOST_SERIALIZATION_NVP(product_gene_ratio_avrg);
    ar & BOOST_SERIALIZATION_NVP(product_gene_ratio_max);
    ar & BOOST_SERIALIZATION_NVP(start_nmbr_genes);
    ar & BOOST_SERIALIZATION_NVP(pop_seed_used);
    ar & BOOST_SERIALIZATION_NVP(evo_seed_used);
    ar & BOOST_SERIALIZATION_NVP(env_seed_used);
    ar & BOOST_SERIALIZATION_NVP(savedirectory);
    ar & BOOST_SERIALIZATION_NVP(grace_par_file);
    ar & BOOST_SERIALIZATION_NVP(Cell::total_number_cells);
  }
};
ostream & operator <<(ostream &, PetriDish &);
//bool operator<(const Cell, const Cell);

bool Cell_Production_Compare(const Cell* const& , const Cell* const& );
bool DishSortPredicate(const PetriDish * lhs, const PetriDish * rhs);



#endif


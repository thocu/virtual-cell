/*****************--ToDo's--************************

g++ -O0 -ggdb cell.cpp gene.cpp genestructs.cpp my_random.cpp
valgrind a.out

mutaties op verschillende parameters aanpassen: let op (on)mogelijkheid 
negatieve waarden & incrementele ophoging... / random toewijzing , vragen aan Paulien.
*****************--ToDo's--************************/

#include <vector>
#include <utility>
#include <list>
#include <fstream> 
#include <iomanip>
#include <string>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>

//#include "gene.hpp"
//#include "genestructs.hpp"

#ifndef CELL_HPP_
#define CELL_HPP_

#include "my_random.hpp"
#include "gene_iter_wrapper.hpp"

// #define Perm 0.1
// #define startAin 0.5
// #define startXin 0.5
// #define TARGETA 1.
// #define TARGETX 1.
// #define N 4. // # of ATP molecules produced in catabolism from 1 glucose
// #define ASSAYS 3

// #define TF_RAT 2.    //1.
// #define PUMP_RAT 1.  //1.
// #define ENZ_RAT 1.   //1.

using namespace std;
namespace bf = boost::filesystem;

class Cell{
 public:
  static int total_number_cells;
  Cell();
  Cell(double,string);
  Cell(int,string,string);
  Cell(string);
  Cell(const Cell&);
  Cell(Cell&,string);
  Cell(Cell&,int,string);
  ~Cell();

  void mutateGenes(double,double,double);  //dup,del,point
  void mutateGenes2(double,double,double);  //dup,del,point
  void mutateGenes3(double,double,double,double);
  void mutateGenes4(double,double,double,double,double,double); //ins, dup,del,point,wgd,mutation rate
  void mutateGenes5(double,double,double,double); //dup,del,point,wgd
  void mutateGenes6(double,double,double,double,double); //ins,dup,del,point,wgd
  void mutateGenes7(double,double,double,double,double,double,double,double,double,double); //ins,dup,del,point,sg,gcr,polyploidization,nothing,gcr_scaling
  void mutateAgene(double);

  void reset_concentration();
  void reset_update(){ update = true;};
  void setW(TF&);
  bool CalcNextProt();
  void SetNewProt();
  double PassiveA(double);
  double Catabolise(Enzyme&);
  double Anabolise(Enzyme&);
  double Pumping(Pump&, double);
  bool CalcNextMol();
  void SetNewMol();
  void SetNewA();

  void NewVals();		
  bool Update(int);

  string ConcPoint();
  void printConc(string,double);
  string RatePoint();
  void printRates(string,double);
  void tfAttributes();
  void colourGenesStrength();
  void colourGenesFitness();
  void graphScheme(string,string);
  void namedGraph(string,string);
  void Graph(string);
  void geneContributionGraph(string);
  void genomeOrderGraph(string);
  string toString();
  void toFile(string savedir);

  Gene_iter_wrapper getGene(int);
  void setChild(Cell *);
  bool noChildren();
  void removeChild(Cell *);
  void removeParent();
  void informChildren();
  void informParent();
  const Cell * getParent(){return parent;};
  Cell * getAncestor(){return parent;};
  const list<Cell *> getChildren(){return children;};
  list<Cell *> getOffspring(){return children;};
  list<Cell *>::iterator getChildIter(){return child_iter;};
  Cell * findRoot();

  int get_number()const {return number;};
  int get_parent_number(){return parent_number;};
  int getnmbr() {return number;};
  int get_nmbr_genes(){ return genes;};
  int get_nmbr_tfs(){ return tf_list.size();};
  int get_nmbr_enzymes(){ return enzyme_list.size();};
  int get_nmbr_pumps(){ return pump_list.size();};
  int gene_nmbr_max();

  int get_dels(){ return dels;};
  int get_dups(){ return dups;};
  int get_points(){ return points;};
  int get_wgds() { return wgds;};
  int get_major_dels(){ return majordels;};
  int get_major_dups(){ return majordups;};
  int get_major_ins(){ return majorins;};
  int get_tf_dels(){ return tfdels;};
  int get_tf_dups(){ return tfdups;};
  int get_tf_points(){ return tfpoints;};
  int get_enzyme_dels(){ return enzymedels;};
  int get_enzyme_dups(){ return enzymedups;};
  int get_enzyme_points(){ return enzymepoints;};
  int get_pump_dels(){ return pumpdels;};
  int get_pump_dups(){ return pumpdups;};
  int get_pump_points(){ return pumppoints;};

  int get_pump_op_muts(){ return pump_op_muts;};
  int get_enz_op_muts(){ return enz_op_muts;};
  int get_tf_op_muts(){ return tf_op_muts;};
  int get_tf_bind_muts(){ return tf_bind_muts; };
  list<list<pair<int,int> > > get_duplication_pairs() {return duplication_pairs; };

  double tfs_contribution();
  double enzymes_contribution();
  double pumps_contribution();

  double FitnessAssay();
  double splitFitness(int);
  double penalize(double);
  void store_fitness(int);
  void store_scaled_fitness(double);
  double get_fitness()const{return _fitness;};
  bool is_updating(){return update;};
  bool get_error(){return error;};

  //  void Cell::store_expression_A_X(int i){
  void store_expression_A_X(int i){
    expr_A_X[i] = make_pair(A_in - TARGETA, X_in -TARGETX);
  }
  //  pair<double,double> Cell::split_expression(int i){
  pair<double,double> split_expression(int i){
    return expr_A_X[i];
  }
  void store_production(int);
  double splitAnabolite(int i){
    return anabolite[i];
  }
  double ReproductionAssay();
  void store_scaled_production(double);
  double get_production()const {return _production;};

  void setSurvived(){ survived = true; };
  void killCell(){ dead = true;};
  bool isDead(){ return dead; };
  void storePosition(int row,int col){ 
    x_position = row;
    y_position = col;
  }

  list<Gene_iter_wrapper> compare_genomes(Cell *);
  void reconstruct_network();
  void order_genes();
  bool consistent_genome();
  void CleanDupPairs();  // remove pairs with non-existing genes from duplication_pair list

  void knockout_gene( list<Gene_iter_wrapper>::iterator);
  void reset_genes_to(Cell *,bool);
  void reset_gene(Enzyme &,Enzyme &);
  void reset_gene(Pump &, Pump &);
  void reset_gene(TF &, TF &);

  list<Enzyme> enzyme_list;
  list<Enzyme>::iterator enzymes_iter;
  list<TF> tf_list;
  list<TF>::iterator tfs_iter;	
  list<Pump> pump_list;
  list<Pump>::iterator pumps_iter;
  list<Gene_iter_wrapper> genome;
  list<Gene_iter_wrapper>::iterator genome_iter;
	
  GeneCollection pool;

  ofstream fout;
  ofstream grout;

  double set_par(Pump &,int,double);
  double set_par(TF &,int,double);
  double set_par(Enzyme &,int,double);
  void pointMutate( list<Gene_iter_wrapper>::iterator );
 
 private:
  int dels,dups,points,majordels,majordups,majorins,wgds;
  int tfdels, tfdups, tfpoints;
  int enzymedels,enzymedups,enzymepoints;
  int pumpdels,pumpdups,pumppoints;
  int enz_op_muts, pump_op_muts, tf_op_muts, tf_bind_muts;
  string name;
  string graphname;
  double A_in; //concentration inside Resource
  double nextA_in;
  double X_in; //concentration energy carrier protein 
  double nextX_in;
  double dAdt;
  double dXdt;
  double prod_rate;        
  double Anabolite;  

  double pump_rate;
  double anabolic_rate;
  double catabolic_rate;
  double leak_rate;
      
  bool survived; 
  bool dead;          
  int x_position;
  int y_position;

  bool error;
  double Euler(double);

  void Insertion(list<Gene_iter_wrapper>::iterator,
		 list<Gene_iter_wrapper>);
  void majorDup(list<Gene_iter_wrapper>::iterator,
		list<Gene_iter_wrapper>); 
  void singleDupInPlace(list<Gene_iter_wrapper>::iterator); 
  pair<int,int> Dup_helper( list<Gene_iter_wrapper>::iterator );
  void majorDel(list<Gene_iter_wrapper>::iterator,
		list<Gene_iter_wrapper>::iterator); 
  void Del_helper( list<Gene_iter_wrapper>::iterator );
  template <class T>
  typename list<T>::iterator Dup(const typename list<T>::iterator&,list<T>& );
  template <class T>
  void Del(typename list<T>::iterator&,list<T>& );

  void pointMutate(TF &);  
  void pointMutate(Enzyme &);
  void pointMutate(Pump &);
 

  void fish_pool();     //all tfs fish in the unresolved gene pool and update their bindlists
  void fish_all_genes(TF &); // a tf that has changed its binding sequence fishes from all genes in the cell
  int Time;
  int number;
  int parent_number;

  Cell* parent;
  list<Cell*> children;
  list<Cell*>::iterator child_iter;

  list<list<pair<int,int> > > duplication_pairs;   
  vector<double> fitness;
  double _fitness;    

  vector<pair<double,double> > expr_A_X;

  vector<double> production;  
  vector<double> anabolite;
  double _production;

  int genes;
  bool nochange;
  bool update;
  
  double runge(double (double), double);
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int){
    ar & BOOST_SERIALIZATION_NVP(enzyme_list); 
    ar & BOOST_SERIALIZATION_NVP(tf_list);
    ar & BOOST_SERIALIZATION_NVP(pump_list);
    ar & BOOST_SERIALIZATION_NVP(genome);

    ar & BOOST_SERIALIZATION_NVP(dels);
    ar & BOOST_SERIALIZATION_NVP(dups);
    ar & BOOST_SERIALIZATION_NVP(points);
    if(!BEFORE_WGD){
      ar & BOOST_SERIALIZATION_NVP(wgds);
    }
    ar & BOOST_SERIALIZATION_NVP(majordels);
    ar & BOOST_SERIALIZATION_NVP(majordups);
    ar & BOOST_SERIALIZATION_NVP(majorins);

    ar & BOOST_SERIALIZATION_NVP(tfdels);
    ar & BOOST_SERIALIZATION_NVP(tfdups);
    ar & BOOST_SERIALIZATION_NVP(tfpoints);
    ar & BOOST_SERIALIZATION_NVP(enzymedels);
    ar & BOOST_SERIALIZATION_NVP(enzymedups);
    ar & BOOST_SERIALIZATION_NVP(enzymepoints);
    ar & BOOST_SERIALIZATION_NVP(pumpdels);
    ar & BOOST_SERIALIZATION_NVP(pumpdups);
    ar & BOOST_SERIALIZATION_NVP(pumppoints);
    ar & BOOST_SERIALIZATION_NVP(enz_op_muts);
    ar & BOOST_SERIALIZATION_NVP(pump_op_muts);
    ar & BOOST_SERIALIZATION_NVP(tf_op_muts);
    ar & BOOST_SERIALIZATION_NVP(tf_bind_muts);

    ar & BOOST_SERIALIZATION_NVP(name);
    ar & BOOST_SERIALIZATION_NVP(graphname) ;
    /* minimizing memory allocation */
    // ar & BOOST_SERIALIZATION_NVP(A_in);
    // ar & BOOST_SERIALIZATION_NVP(nextA_in); 
    // ar & BOOST_SERIALIZATION_NVP(X_in);
    // ar & BOOST_SERIALIZATION_NVP(nextX_in); 
    //ar & BOOST_SERIALIZATION_NVP(dAdt);
    //ar & BOOST_SERIALIZATION_NVP(dXdt);
    //ar & BOOST_SERIALIZATION_NVP(prod_rate);
    //ar & BOOST_SERIALIZATION_NVP(Anabolite);
    ar & BOOST_SERIALIZATION_NVP(survived) ;
    ar & BOOST_SERIALIZATION_NVP(dead);
    ar & BOOST_SERIALIZATION_NVP(x_position);
    ar & BOOST_SERIALIZATION_NVP(y_position);
    ar & BOOST_SERIALIZATION_NVP(Time);
    ar & BOOST_SERIALIZATION_NVP(number) ;
    ar & BOOST_SERIALIZATION_NVP(parent_number);
    ar & BOOST_SERIALIZATION_NVP(parent) ;    
    ar & BOOST_SERIALIZATION_NVP(children);
    ar & BOOST_SERIALIZATION_NVP(duplication_pairs);
    ar & BOOST_SERIALIZATION_NVP(fitness);
    ar & BOOST_SERIALIZATION_NVP(_fitness);
    ar & BOOST_SERIALIZATION_NVP(production);
    //    ar & BOOST_SERIALIZATION_NVP(anabolite);
    ar & BOOST_SERIALIZATION_NVP(_production);
    ar & BOOST_SERIALIZATION_NVP(genes);
    ar & BOOST_SERIALIZATION_NVP(update);    
    ar & BOOST_SERIALIZATION_NVP(Gene::total_nmbr_genes);
  }
};

ostream & operator <<(ostream &, Cell &);
ostream & operator <<(ostream &, Gene_iter_wrapper &);

#endif

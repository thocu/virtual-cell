
#include <string>
#include <iostream>
#include <time.h>
//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/xml_oarchive.hpp>
//#include <boost/archive/xml_iarchive.hpp>

#include <boost/program_options.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <boost/serialization/nvp.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/list.hpp>

#ifndef TEMP_HPP_
#define TEMP_HPP_

using namespace std;

enum Type {NOTYPE,ENZYME,PUMP,TrF};
enum Ligand {NO,A,X};
extern double A_out;
extern time_t open_time;


//petridish.cpp
extern int LIFETIME;
extern double MEASURESTEP;
extern double MUTATION_RATE;
extern double CHANGE_RATE;

extern int PER_GENE_MUTATION;
extern bool SPARSE;
extern bool DEATH;
extern bool ENV_SKEWED;
extern int REPRODUCE;
extern bool _3ENV;
extern bool LOWPROFILE;
extern int TREE_CUTOFF;
extern int SAVETIME, ANALYZE_TIME;
extern int STOPSAVE;
extern int GENERATIONS,POP_SEED,EVO_SEED,ENV_SEED;
extern double CHANGE_THRESHOLD, H, ERROR_RATE, STEP_REDUCTION;
extern double ENV_CHANGE_FIT_MIN;
extern bool ENV_SWITCHING;
extern bool CHANGE_ENV;
extern double RANK_CUT_OFF;
extern bool NO_TARGET_CONC;
extern bool RESET;
//cell.cpp

extern double INS,DUP,DEL,POINT,WGD;
extern double GCR,SG,POLYP,NOTHING;
extern double MAX_GCR_FRAC;
extern double TF_RAT, PUMP_RAT, ENZ_RAT;
extern double Perm, startAin, startXin, TARGETA, TARGETX, N;
extern int ASSAYS;
extern double Degr,STARTCONC;
extern double GEO_DIST_LENGTH,EXP_DIST_EVENTS;
extern bool BEFORE_WGD;
extern double SIZE_PENALTY;
extern bool RESTRICT_SIZE;
extern int SIZE_MAX;
#endif



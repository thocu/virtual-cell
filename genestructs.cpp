#include "gene.hpp"
#include "genestructs.hpp"
#include <boost/serialization/export.hpp>


GeneList::GeneList():genes(0),type(NOTYPE),glist(){cout << "default ctor" << endl;}
GeneList::GeneList(Type typ):  genes(0), type(typ),glist(0){}

GeneList::GeneList(const GeneList& gl):genes(gl.genes),type(gl.type),
				       glist(gl.glist){
}

void GeneList::replaceGenePtr(const Gene * old,Gene * nieuw){
  for(unsigned int i=0; i< getList().size();i++){
    if(glist[i] == old){
      glist[i]= nieuw;
      break;
    }
  }
}

void GeneList::addGene(Gene * gene){
  int k = nmbrGenes();
  int i = getList().size();
  if(gene -> getType() == type){ 
    if(i > k){
      for(int j=0; j<i;j++){
	if(glist[j] == NULL){
	  glist[j] = gene;
	  genes++;
	  break;
	}
      }
    }
    else{ 
      glist.push_back(gene);
      genes++;
    }
		
  }
  else cout<< "tried to add gene of wrong type: expected: " << type << " given: " << gene -> getType() << endl ;		
}
void GeneList::removeGene(Gene * gen){
  for(unsigned int i = 0; i< glist.size();i++){
    if(glist[i]==gen){
      glist[i] = NULL;
      genes--;
      return;
    }
  }
  cout << " gene " << *gen << " not found in this GeneList" << endl;
}

string GeneList::toString(){
	
  ostringstream formatter;
  formatter << '[';
  int k = glist.size();
  for(int i = 0; i< k;i++){
    if(glist[i]){
      formatter << *(glist[i]);
      if(i == k-1) break;
      formatter << ',';
    }
  }
  formatter << ']';
  return formatter.str();;
}

ostream & operator << (ostream & out,GeneList & g){
  out<< g.toString();
  return out;
}

void GeneList::removeAll(){
  glist.clear();
  genes = 0;
}

GeneCollection::GeneCollection(const GeneCollection & col):	tfs(col.tfs),enzymes(col.enzymes),pumps(col.pumps),
								clist(col.clist){
  clist[0] = & tfs;
  clist[1] = & enzymes;
  clist[2] = & pumps;	
}

GeneList & GeneCollection::getGeneList(Type tp){
  if(tp == ENZYME) return enzymes;
  else if(tp == PUMP) return pumps;
  else  return tfs;
}


void GeneCollection::addGene(Gene * g){
  switch(g -> getType()){
  case TrF: tfs.addGene(g);break;
  case ENZYME: enzymes.addGene(g);break;
  case PUMP: pumps.addGene(g);break;
  default: cout << "no matching type for gene " << *g << endl;
  }
  clist[0] = & tfs;
  clist[1] = & enzymes;
  clist[2] = & pumps;
}

void GeneCollection::removeGene(Gene * gen){
  Type typ = gen -> getType();
  GeneList & lst = getGeneList(typ);
  lst.removeGene(gen);
  clist[0] = & tfs;
  clist[1] = & enzymes;
  clist[2] = & pumps;
}

void GeneCollection::replaceGenePtr(const Gene * old, Gene * nieuw){
  Type typ = old -> getType();
  GeneList & lst = getGeneList(typ);
  lst.replaceGenePtr(old,nieuw);
  clist[0] = & tfs;
  clist[1] = & enzymes;
  clist[2] = & pumps;
}

void GeneCollection::empty(){
  tfs.removeAll();
  enzymes.removeAll();
  pumps.removeAll();
  clist[0] = & tfs;
  clist[1] = & enzymes;
  clist[2] = & pumps;
}

string GeneCollection::toString(){
  ostringstream formatter;
  formatter << '{' << *clist[0] << ',' << *clist[1] << ',' << *clist[2] << '}';
  return formatter.str();;
}

ostream & operator << (ostream & out,GeneCollection & g){
  out << g.toString();
  return out;
}

BOOST_CLASS_EXPORT(GeneList);
BOOST_CLASS_EXPORT(GeneCollection);

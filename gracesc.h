//xmgrace class  written by M.Nomura


#ifndef __GRACESC_H
#define __GRACESC_H

#include <string>
#include <vector>
#include <grace_np.h>
//#include "grace_np.h"
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <cstdlib>

namespace gracesc {
     const std::string DEFAULTFILE("default.par");
     const int GRACEBUFFER=16384 ;//2048;
     
     class XmgraceGraph{
     private:
	  unsigned int graph_number;
	  unsigned int linemax;
	  bool newline;
	  std::vector<std::vector<double> > data;
     public:
	  XmgraceGraph(unsigned int number):graph_number(number),linemax(0),newline(true){
	       GracePrintf("g%d hidden false",graph_number);
	  }
	  
	  XmgraceGraph& operator << (const char* command){
	       GracePrintf("with g%d",graph_number);
	       GracePrintf("%s",command);
	       return(*this);
	  }
	  
	  XmgraceGraph& operator << (const double& variable){
	       if(newline){
		    data.push_back(std::vector<double>());  
		    data[data.size()-1].push_back(variable);
		    newline=false;
	       }
	       else{
		    data[data.size()-1].push_back(variable);
	       }
	       
	       return(*this);
	  }
	  
	  void operator << (void (*func)(XmgraceGraph&)){
	       func(*this);
	  }
	  
	  friend void endl(XmgraceGraph&);
	  
	  void clear(void){
	       for(unsigned int i=0;i<linemax;i++){
		    GracePrintf("kill g%d.s%d saveall",graph_number,i);
	       }
	       linemax=0;
	  }
	  
	  void draw(void){
	    if ( data.empty() == false ){
	       unsigned int lasttime=data.size();
	       unsigned int lastline=data[0].size();
	       if(lastline>linemax){
		    linemax=lastline;
	       }
	       for(unsigned int i=0;i<lasttime;i++){
		    for(unsigned int j=1;j<lastline;j++){
		      if(GracePrintf("g%d.s%d point %f, %f",graph_number,j-1,data[i][0],data[i][j]) == -1){
			std::cerr << "could not plot" << std::endl;
		      }
		    }
	       }
	       for(unsigned int i=0;i<lasttime;i++){
		    data[i].clear();
	       }
	       data.clear();
	    }
	  }

	  void drawXYZ(int timepoint){             // we need to send a command with 3 params instead of 2
	    if ( data.empty() == false ){
	       unsigned int lasttime=data.size();
	       unsigned int lastline=data[0].size();
	       if(lastline>linemax){
		    linemax=lastline;
	       }
	       for(unsigned int i=0;i<lasttime;i++){
		    for(unsigned int j=1;j*2<lastline;j++){
		      GracePrintf("g%d.s%d point %f,%f",graph_number, j-1, data[i][0], data[i][(j*2)-1]);
		      //double size = data[i][j*2];
		      //if(size > 0.) size = sqrt(size/500);
		      GracePrintf("g%d.s%d.y1[%d] = %f",graph_number, j-1, timepoint, sqrt(data[i][j*2]/500));
		    }
	       }
	       for(unsigned int i=0;i<lasttime;i++){
		    data[i].clear();
	       }
	       data.clear();
	    }
	  }
       	  void load_commands(const std::string & filename);
	  
     };
     
     class Xmgrace{
     private:
       std::vector<XmgraceGraph> graph;
       int max_graph_size;
       void init( int enable=1, const std::string & parameter_file = "" ){
	 if(enable<0){
	   std::cout << "No attempt to open a grace pipe" << std::endl;
	   return;
	 }
	 if ( parameter_file == ""){ // no parameter file 
	   if(enable){
	     if(GraceOpenVA("xmgrace",GRACEBUFFER,"-nosafe","-noask",NULL) == -1){
	       std::cerr << "Can't run Grace." << std::endl;
	       exit(-1);
	     }
	   }
	   else {
	     if(GraceOpenVA("gracebat",GRACEBUFFER,"-nosafe","-noask","-noprint",NULL) == -1){
	       std::cerr << "Can't run Gracebat." << std::endl;      
	       exit(-1);
	     }
	   }
	   /* if(GraceOpen(GRACEBUFFER)==-1){
	     std::cerr << "Can't run Grace." << std::endl;
	     exit(-1);
	     }*/
	 } 
	 else {
	   if(enable){
	     if( GraceOpenVA("xmgrace",GRACEBUFFER , "-nosafe", "-noask","-param", parameter_file.c_str(), NULL) == -1 ){
	       std::cerr << "Can't run Grace.(" << parameter_file << ")" << std::endl;
	       exit(-1);
	     }
	   }
	   else
	     if( GraceOpenVA("gracebat",GRACEBUFFER , "-nosafe", "-noask","-noprint","-param", parameter_file.c_str(), NULL) == -1 ){
	       std::cerr << "Can't run GraceBat.(" << parameter_file << ")" << std::endl;
	       exit(-1);
	     }
	 }
	       
	 GracePrintf("focus off");
       }

     public:
       Xmgrace(){};
       Xmgrace(int enable, unsigned int size=0):max_graph_size(-1){
	 init(enable);
	 add(size);
       }
       Xmgrace( int enable, const std::string &parameter_file = DEFAULTFILE,unsigned int size=0 ):max_graph_size(-1){
	 init(enable,parameter_file);
	 add(size);
       }
	  
	 virtual ~Xmgrace (){ GraceClosePipe();}
	  
	 static  void close(){ GraceClose();}

	 void add(unsigned int size){
	   for(unsigned int i=0;i<size;i++){
	     max_graph_size++;
	     graph.push_back(XmgraceGraph(max_graph_size));
	   }
	 }
	 
	 Xmgrace& operator << (const char* command){
	   GracePrintf("%s",command);
	   return(*this);
	 }
	 
	 XmgraceGraph& operator [] (unsigned int number){
	   return graph[number];
	 }
	 
	 bool open(void){
	   return GraceIsOpen();
	 }
	 
	 void load_commands(const std::string & filename);
	 
	 void redraw(void){
	   GracePrintf("redraw");
	 }
	 
     };
     
     void endl(XmgraceGraph&);
     
}

#endif

#include "gracesc.h"
#include <fstream>
#include <iostream>
#define BUFSIZE 512

namespace gracesc {
     void endl(XmgraceGraph& graph){
	  graph.newline=true;
     }
     
     void read_command_file(const std::string &filename){
	  std::ifstream ifs(filename.c_str(), std::ios::in);    
	  if (ifs.fail()){
	       std::cerr << "Error(load commands): unable to open file(" << filename << ")" << std::endl;
	       exit(1);
	  }
	  char buffer[BUFSIZE];
	  while (!ifs.eof()){
	       ifs.getline(buffer, BUFSIZE);
	       GraceCommand(buffer);
	  }
	  ifs.close();
     }
     
     void XmgraceGraph::load_commands(const std::string & filename){
	  GracePrintf("with g%d",graph_number);
	  read_command_file(filename);
     }
     
     void Xmgrace::load_commands(const std::string & filename){
	  read_command_file(filename);
     }
     
}
